QT += network
QT -= gui

CONFIG += c++14

SOURCES += \
    main.cpp \
    server.cpp \
    client.cpp \
    dataserver.cpp

HEADERS += \
    server.h \
    client.h \
    dataserver.h

include ($$PWD/../Client/libs/accountinfo/accountinfo.pri);
