#include <QtCore/QCoreApplication>
#include <QDebug>

#include "../fm_namespace.h"

#include "server.h"

QList <AccountInfo> accountList;

bool readAccountList()
{
    QFile file(QDir::current().absoluteFilePath("data.profile"));
    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream;
        stream.setDevice(&file);
        int lenght;
        stream >> lenght;
        for (int i = 0; i < lenght; i++){
            AccountInfo a;
            stream >> a;
            accountList.push_back(a);
        }
        file.close();
        return true;
    }
    return false;
}

int main(int argc, char **argv){
    QCoreApplication app(argc, argv);

    Server *server = new Server;
    if (! readAccountList()){
        server->closeServer();
        readAccountList();
    }
    server->startServer()
            ? qDebug() << "Server started"
            : qDebug() << "Server not started";

    return app.exec();
}
