#ifndef SERVER_H
#define SERVER_H

#include "client.h"
#include "dataserver.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QCoreApplication>
#include <QtNetwork/QTcpServer>

#define N 15

class ClientList {
public:
    int number;
    QList <Client*> list;

    ClientList(){
        number = 0;
        list.clear();
    }
    void addClient(QTcpSocket* socket, QObject *parent){
        number++;
        list.push_back(new Client(socket, parent));
    }
};

class Server : public QObject
{
    Q_OBJECT

private:
    QTcpServer *server;
    ClientList* clientList[N];
    DataServer *data;
    QTimer *timer;

    int minimumList();

public:
    Server();
    ~Server();

    Client *localClient;
    bool startServer();

private slots:
    void addNewClient();
    void testClientList();

public slots:
    void closeServer();
};

#endif // SERVER_H
