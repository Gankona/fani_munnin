#include "client.h"

//pinterest

extern QList <AccountInfo> accountList;

Client::Client(QTcpSocket *socket, QObject *parent) : QObject(parent), socket(socket)
{
    stream.setDevice(socket);
    QObject::connect(this->socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}

void Client::readyRead()
{
    int flag;
    stream >> flag;
    qDebug() << flag;
    switch (Flags(flag)) {
    case Flags::ping:
        isConnected = true;
        break;

    case Flags::testLogin:{
        QString login;
        stream >> login;
        foreach (AccountInfo a, accountList)
            if (a.login == login){
                stream << int(Flags::testLogin)
                       << int(Message::loginPresent);
                return;
            }
        stream << int(Flags::testLogin)
               << int(Message::loginFree);
        break;
    }

    case Flags::getProfile:{
        QString login;
        stream >> login;
        foreach (AccountInfo a, accountList)
            if (a.login == login){
                stream << int(Flags::getProfile)
                       << a;
                qDebug() << a.login << a.password << a.isNoName;
            }
        break;
    }

    case Flags::testEmail:{
        QString email;
        stream >> email;
        foreach (AccountInfo a, accountList)
            if (a.email == email){
                stream << int(Flags::testEmail)
                       << int(Message::emailPresent);
                return;
            }
        stream << int(Flags::testEmail)
               << int(Message::emailFree);
        break;
    }

    case Flags::registration:{
        AccountInfo account;
        stream >> account;
        qDebug() << "case Flags::registration:" << account.login << account.password;
        foreach (AccountInfo a, accountList){
            if (a.login == account.login){
                stream << int(Flags::registration)
                       << int(Message::loginPresent);
                return;
            }
            if (a.email == account.email){
                stream << int(Flags::registration)
                       << int(Message::emailPresent);
                return;
            }
            if (a.telephon == account.telephon){
                stream << int(Flags::registration)
                       << int(Message::telephonPresent);
                return;
            }
        }
        accountList.push_back(account);
        stream << int(Flags::registration)
               << int(Message::successfulRegistraton);
        break;
    }

    case Flags::login:{
        qDebug() << "login";
        QString login;
        QString password;
        stream >> login >> password;
        foreach (AccountInfo a, accountList)
            if (a.login == login){
                if (a.password == password)
                    stream << int(Flags::login)
                           << int(Message::successfulLogin)
                           << a.login
                           << a.lastUpdate;
                else
                    stream << int(Flags::login)
                           << int(Message::passwordWrong);
                return;
            }
        stream << int(Flags::login)
               << int(Message::accountNotPresent);
        break;
    }

    default:
        stream << int(Flags::unknown);
    }
}

void Client::ping(){}
