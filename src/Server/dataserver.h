#ifndef DATASERVER_H
#define DATASERVER_H

#include <QtCore/QByteArray>
#include <QtCore/QDataStream>
#include <QtCore/QFile>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QString>

class DataServer
{
public:
    static void writeFiles(QString login, QString name, QByteArray data);
    static QByteArray readFiles(QString login, QMap <QString, QDateTime> map);
    static QList <QString> testFilesForDownload(QString login, QMap <QString, QDateTime> map);
};

#endif // DATASERVER_H
