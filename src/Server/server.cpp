#include "server.h"

extern QList <AccountInfo> accountList;

Server::Server()
{
    for (int i = 0; i < N; i++)
        clientList[i] = new ClientList;
    data = new DataServer;
}

Server::~Server()
{
    delete data;
    for (int i = 0; i < N; i++)
        delete clientList[i];
}

void Server::closeServer()
{
    QFile file(QDir::current().absoluteFilePath("data.profile"));
    file.open(QIODevice::WriteOnly);
    QDataStream stream;
    stream.setDevice(&file);
    stream << accountList.length();
    foreach (AccountInfo a, accountList){
        stream << a;
        qDebug() << a.login << a.password << a.isNoName;
    }
    file.close();
    qDebug() << "close Server";
}

bool Server::startServer()
{
    server = new QTcpServer(this);
    QObject::connect(server, SIGNAL(newConnection()), SLOT(addNewClient()));
    qDebug() << "server create at " << QDateTime::currentDateTime().toString();

    timer = new QTimer(this);
    timer->setInterval(30000);
    timer->start();

    QObject::connect(qApp, SIGNAL(destroyed(QObject*)), this, SLOT(closeServer()));
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(closeServer()));

    return server->listen(QHostAddress::Any, 32802);
}

void Server::addNewClient()
{
    clientList[minimumList()]->addClient(server->nextPendingConnection(), this);
    qDebug() << "CONNECT!";
}

void Server::testClientList(){}

int Server::minimumList()
{
    int min(0);
    for (int i = 0; i < N; i++)
        if (clientList[i]->number < clientList[min]->number)
            min = i;
    return min;
}
