#ifndef CLIENT_H
#define CLIENT_H

#include "../fm_namespace.h"
#include "dataserver.h"
#include "accountinfo.h"

#include <QtCore/QList>
#include <QtCore/QDir>
#include <QtCore/QDataStream>
#include <QtNetwork/QTcpSocket>

using namespace FM::Flags;
using namespace FM::Message;

class Client : public QObject
{
    Q_OBJECT
private:
    QDataStream stream;
    QTcpSocket *socket;

public:
    Client(QTcpSocket *socket = nullptr, QObject *parent = 0);
    bool isConnected;

    void ping();

signals:

private slots:
    void readyRead();
};

#endif // CLIENT_H
