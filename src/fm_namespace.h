#ifndef FM_NAMESPACE
#define FM_NAMESPACE

#include <qt5/QtCore/QMetaType>

namespace FM {

    namespace Message {

        enum Message {
            //Registration
            //info
            successfulRegistraton = 0x0000,
            loginFree = 0x0001,
            emailFree = 0x0002,
            //warning
            insecurePassword = 0x0003,
            emailIncorrect   = 0x0004,
            //error
            loginPresent     = 0x0005,
            emailPresent       = 0x0006,
            passwordIncorrect  = 0x0007,
            passwordDoNotMatch = 0x0008,
            telephonPresent    = 0x0009,

            //Login
            //info
            successfulLogin  = 0x0010,
            successfulLogout = 0x0011,
            //warning
            profileInfoNotPresent = 0x0012,
            //error
            accountNotPresent = 0x0013,
            passwordWrong     = 0x0014,
            emailNotPresent   = 0x0015,

            //EditProfile

            //ServerConnection
            //info
            connectToServer = 0x0030,
            //warning
            //error
            serverNotFound = 0x0031,

            //Directory
            cannotOpenDirectory = 0x0040,

            //Other
            unknownError = 0xFFF0,
        };
    }

    namespace Flags {
        enum Flags {
            //server flags
            ping = 0x4000,
            serverNotFound = 0x4001,

            //registration
            testLogin = 0x4010,
            testEmail = 0x4011,
            registration = 0x4012,

            //login
            login = 0x4020,
            getProfile = 0x4021,
            logout = 0x4022,

            //type message
            information = 0x4030,
            critical = 0x4031,
            warning = 0x4032,
            debug = 0x4033,

            //space enum
            showInMessageManagerBox  = 0x4040,
            showInDestination = 0x4041,
            showInMessageManagerBar  = 0x4042,

            //other
            unknown = 0x4FFF0,
        };
    }
}

Q_DECLARE_METATYPE(FM::Flags::Flags)

Q_DECLARE_METATYPE(FM::Message::Message)


#endif
