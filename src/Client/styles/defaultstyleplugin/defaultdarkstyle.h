#ifndef DEFAULTDARKSTYLE_H
#define DEFAULTDARKSTYLE_H

#include <QtWidgets/QProxyStyle>

QT_BEGIN_NAMESPACE
class QPalette;
QT_END_NAMESPACE

class DefaultDarkStyle : public QProxyStyle
{
public:
    DefaultDarkStyle(){}

    void polish(QPalette &palette) Q_DECL_OVERRIDE;
};

#endif // DEFAULTDARKSTYLE_H
