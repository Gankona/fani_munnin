#ifndef DEFAULTNAVYSTYLE_H
#define DEFAULTNAVYSTYLE_H

#include <QtWidgets/QProxyStyle>

QT_BEGIN_NAMESPACE
class QPalette;
QT_END_NAMESPACE

class DefaultNavyStyle : public QProxyStyle
{
public:
    DefaultNavyStyle();

    void polish(QPalette &palette) Q_DECL_OVERRIDE;
};

#endif // DEFAULTNAVYSTYLE_H
