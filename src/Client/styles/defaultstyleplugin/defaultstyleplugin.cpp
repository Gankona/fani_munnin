#include "defaultstyleplugin.h"

#include "defaultdarkstyle.h"
#include "defaultnavystyle.h"
#include "defaultlightstyle.h"
#include "defaultwinterstyle.h"

QStyle *DefaultLightPlugin::create(const QString &key)
{
    if (key.toLower() == "default-light")
        return new DefaultLightStyle;
    else if (key.toLower() == "default-dark")
        return new DefaultDarkStyle;
    else if (key.toLower() == "default-navy")
        return new DefaultNavyStyle;
    else if (key.toLower() == "default-winter")
        return new DefaultWinterStyle;
    return 0;
}
