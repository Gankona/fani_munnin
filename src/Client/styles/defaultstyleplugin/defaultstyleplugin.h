#ifndef DEFAULTLIGHTPLUGIN_H
#define DEFAULTLIGHTPLUGIN_H

#include <QStylePlugin>

QT_BEGIN_NAMESPACE
class QStyle;
QT_END_NAMESPACE

class DefaultLightPlugin : public QStylePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QStyleFactoryInterface"
                      FILE "defaultstyleplugin.json")

public:
    DefaultLightPlugin(){}

    QStyle *create(const QString &key) Q_DECL_OVERRIDE;
};

#endif // DEFAULTLIGHTPLUGIN_H
