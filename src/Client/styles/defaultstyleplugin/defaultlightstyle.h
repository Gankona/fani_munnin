#ifndef DEFAULTLIGHTSTYLE_H
#define DEFAULTLIGHTSTYLE_H

#include <QtWidgets/QProxyStyle>

QT_BEGIN_NAMESPACE
class QPalette;
QT_END_NAMESPACE

class DefaultLightStyle : public QProxyStyle
{
    Q_OBJECT
public:
    DefaultLightStyle(){}

    void polish(QPalette &palette) Q_DECL_OVERRIDE;
};

#endif // DEFAULTLIGHTSTYLE_H
