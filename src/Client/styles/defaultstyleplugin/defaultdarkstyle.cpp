#include "defaultdarkstyle.h"

void DefaultDarkStyle::polish(QPalette &palette)
{
    palette.setBrush(QPalette::Window, Qt::darkGray);
    palette.setBrush(QPalette::Text, Qt::cyan);
}
