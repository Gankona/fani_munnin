#include "defaultnavystyle.h"

void DefaultNavyStyle::polish(QPalette &palette)
{
    palette.setBrush(QPalette::Window, Qt::cyan);
    palette.setBrush(QPalette::Text, Qt::white);
}
