#-------------------------------------------------
#
# Project created by QtCreator 2016-02-03T14:01:55
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = defaultstyleplugin
TEMPLATE = lib
CONFIG += plugin

include($$PWD/../../path.pri)

DESTDIR = $$BIN_PATH/styles

SOURCES += defaultstyleplugin.cpp \
    defaultlightstyle.cpp \
    defaultdarkstyle.cpp \
    defaultnavystyle.cpp \
    defaultwinterstyle.cpp

HEADERS += defaultstyleplugin.h \
    defaultlightstyle.h \
    defaultdarkstyle.h \
    defaultnavystyle.h \
    defaultwinterstyle.h
DISTFILES += defaultstyleplugin.json
