#ifndef DEFAULTWINTERSTYLE_H
#define DEFAULTWINTERSTYLE_H

#include <QtWidgets/QProxyStyle>

QT_BEGIN_NAMESPACE
class QPalette;
QT_END_NAMESPACE

class DefaultWinterStyle : public QProxyStyle
{
public:
    DefaultWinterStyle();

    void polish(QPalette &palette) Q_DECL_OVERRIDE;
};

#endif // DEFAULTWINTERSTYLE_H
