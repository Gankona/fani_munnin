#ifndef MSWITCHERPOS_H
#define MSWITCHERPOS_H

#include "mservice.h"

#include <QtCore/qglobal.h>
#include <QtCore/QObject>
#include <QtWidgets/QWidget>
#include <QDebug>

#if defined(MSWITCHERPOS_LIBRARY)
#  define MSWITCHERPOSSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MSWITCHERPOSSHARED_EXPORT Q_DECL_IMPORT
#endif

class MSWITCHERPOSSHARED_EXPORT MSwitcherPos
        : public QWidget
{
    Q_OBJECT
private:
    QPushButton *yesButton;
    QPushButton  *noButton;
    QSize widgetSize;
    QSize buttonSize;
    bool currentPos;

public:
    MSwitcherPos(QWidget *parent = nullptr,
                 QString no = "no",
                 QString yes = "yes",
                 QSize size = QSize(75, 25));
    void setSwitcherPos(bool isTrue);
    bool isTrueSwitchPos();

private slots:
    void clickToButton();

signals:
    void buttonPressed(bool);
};

#endif // MSWITCHERPOS_H
