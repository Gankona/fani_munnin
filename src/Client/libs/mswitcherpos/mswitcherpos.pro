#-------------------------------------------------
#
# Project created by QtCreator 2016-01-26T21:49:24
#
#-------------------------------------------------

QT       += widgets

TARGET = mswitcherpos
TEMPLATE = lib


include($$PWD/../../path.pri)
DESTDIR = $$BIN_PATH/libs
include($$SRC_PATH/libs/mservice/mservice.pri)

DEFINES += MSWITCHERPOS_LIBRARY

SOURCES += mswitcherpos.cpp

HEADERS += mswitcherpos.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    mswitcherpos.pri
