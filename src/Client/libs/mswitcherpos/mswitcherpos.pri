include($$PWD/../../path.pri)
include($$SRC_PATH/libs/mservice/mservice.pri)

isEmpty(mswitcherpos.pri){
    LIBS += -L$$BIN_PATH/libs/ -lmswitcherpos
    INCLUDEPATH += $$SRC_PATH/libs/mswitcherpos
    DEPENDPATH  += $$SRC_PATH/libs/mswitcherpos
}
