#include "mswitcherpos.h"

MSwitcherPos::MSwitcherPos(QWidget *parent, QString no,
                           QString yes, QSize size)
    : QWidget(parent), widgetSize(size), buttonSize(size)
{
    buttonSize.setWidth(buttonSize.width()/2);
    yesButton = MService::flatButton(this, yes, buttonSize);
    noButton  = MService::flatButton(this, no,  buttonSize);
    noButton->move(0, 0);
    yesButton->move(buttonSize.width(), 0);
    setSwitcherPos(true);

    QObject::connect(yesButton, SIGNAL(clicked(bool)), this, SLOT(clickToButton()));
    QObject::connect(noButton, SIGNAL(clicked(bool)), this, SLOT(clickToButton()));
}

void MSwitcherPos::clickToButton()
{
    (QPushButton*)sender() == yesButton ? setSwitcherPos(true)
                                        : setSwitcherPos(false);
    qDebug() << currentPos;
    emit buttonPressed(currentPos);
}

void MSwitcherPos::setSwitcherPos(bool isTrue)
{
    currentPos = isTrue;
    yesButton->setChecked(currentPos);
    noButton->setChecked(! currentPos);
}

bool MSwitcherPos::isTrueSwitchPos()
{
    return currentPos;
}
