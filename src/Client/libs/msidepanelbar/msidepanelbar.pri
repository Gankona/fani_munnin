include($$PWD/../../path.pri)

include($$SRC_PATH/libs/mservice/mservice.pri)

isEmpty(msidepanelbar.pri){
    LIBS += -L$$BIN_PATH/libs/ -lmsidepanelbar
    INCLUDEPATH += $$SRC_PATH/libs/msidepanelbar
    DEPENDPATH  += $$SRC_PATH/libs/msidepanelbar
}
