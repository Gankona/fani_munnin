#include "msidepanelbar.h"

MSidePanelBar::MSidePanelBar(QWidget *parent, SideEnum side, int sizePixel)
    : QFrame(parent), side(side), sizePixel(sizePixel)
{
    if ( ! (side == SideEnum::Left || side == SideEnum::Right
            || side == SideEnum::Top || side == SideEnum::Bottom))
        this->side = SideEnum::Left;
    this->side == SideEnum::Left || this->side == SideEnum::Right
            ? setFixedWidth (sizePixel)
            : setFixedHeight(sizePixel);
    repaint();
}

int MSidePanelBar::setNewButton(bool isUp, QIcon pixmap, QString toolTip)
{
    int min(0);
    int max(-1);
    foreach (int n, buttonMap.keys()){
        if (n > max) max = n;
        if (n < min) min = n;
    }
    int result;
    isUp ? result = ++max : result = --min;
    buttonMap.insert(result, MService::flatSquarePictureButton(pixmap, this,
                                                               toolTip, getButtonSize()));
    buttonMap.last()->show();
    QObject::connect(buttonMap.last(), SIGNAL(clicked(bool)), this, SLOT(clickToButton()));
    int w(0);
    int h(0);
    side == 1 || side == 4
            ? result >= 0 ? h = sizePixel*result : h = this->size().height() + sizePixel*result
            : result >= 0 ? w = sizePixel*result : w = this->size().width()  + sizePixel*result;
    buttonMap.last()->move(w, h);
    return result;
}

void MSidePanelBar::resizeEvent(QResizeEvent *e)
{
    int w(0);
    int h(0);
    foreach (int i, buttonMap.keys()){
        side == 1 || side == 4 == 0
                ? i >= 0 ? h = sizePixel*i : h = e->size().height() + sizePixel*i
                : i >= 0 ? w = sizePixel*i : w = e->size().width()  + sizePixel*i;
        buttonMap.value(i)->move(w, h);
    }
}

void MSidePanelBar::clickToButton()
{
    foreach (int i, buttonMap.keys())
        buttonMap.value(i) == (QPushButton*)sender()
                ? buttonMap.value(i)->setChecked(true), setCurrentWidget(i)
                : buttonMap.value(i)->setChecked(false);
}

QSize MSidePanelBar::getButtonSize()
{
    return QSize(sizePixel, sizePixel);
}

SideEnum MSidePanelBar::getSide()
{
    return side;
}

void MSidePanelBar::setSide(SideEnum side)
{
    if ( ! (side == SideEnum::Left || side == SideEnum::Right
            || side == SideEnum::Top || side == SideEnum::Bottom))
        return;
    this->side = side;
    repaint();
}

QMap <int, QPushButton*> MSidePanelBar::getMap()
{
    return buttonMap;
}
