#-------------------------------------------------
#
# Project created by QtCreator 2016-01-26T21:48:11
#
#-------------------------------------------------

CONFIG += c++14

QT       += widgets

TARGET = msidepanelbar
TEMPLATE = lib


include($$PWD/../../path.pri)
include($$SRC_PATH/libs/mservice/mservice.pri)

DESTDIR = $$BIN_PATH/libs

DEFINES += MSIDEPANELBAR_LIBRARY

SOURCES += msidepanelbar.cpp

HEADERS += msidepanelbar.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    msidepanelbar.pri
