#ifndef MSIDEPANELBAR_H
#define MSIDEPANELBAR_H

#include "mservice.h"

#include <QDebug>
#include <QtCore/qglobal.h>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>

#if defined(MSIDEPANELBAR_LIBRARY)
#  define MSIDEPANELBARSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MSIDEPANELBARSHARED_EXPORT Q_DECL_IMPORT
#endif

class MSIDEPANELBARSHARED_EXPORT MSidePanelBar : public QFrame
{
    Q_OBJECT
private:
    SideEnum side;
    int sizePixel;
    QMap <int, QPushButton*> buttonMap;

    void resizeEvent(QResizeEvent *e);

public:
    MSidePanelBar(QWidget *parent = nullptr,
                  SideEnum side = SideEnum::Left,
                  int sizePixel = 25);
    QSize getButtonSize();
    SideEnum getSide();
    int setNewButton(bool isUp,
                     QIcon pixmap,
                     QString toolTip);
    void setSide(SideEnum side);
    QMap <int, QPushButton*> getMap();

private slots:
    void clickToButton();

signals:
    void setCurrentWidget(int);
};

#endif // MSIDEPANELBAR_H
