#ifndef ACCOUNTINFO_H
#define ACCOUNTINFO_H

#include <QtCore/qglobal.h>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QMultiMap>
#include <QtCore/QDataStream>
#include <QDebug>

#if defined(ACCOUNTINFO_LIBRARY)
#  define ACCOUNTINFOSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ACCOUNTINFOSHARED_EXPORT Q_DECL_IMPORT
#endif

class ACCOUNTINFOSHARED_EXPORT AccountInfo
{
public:
    AccountInfo();

    QString name;
    QString login;
    QString password;
    QString helpWord;
    QString telephon;
    QString email;
    QDateTime regTime;
    QDateTime lastUpdate;
    //name devise and time to login on it
    QMultiMap <QString, QDateTime> timeLogin;
    //if true, it means that this is default account
    bool isNoName;

    //server
    QMap <QString, int> hostList;
    QString mainHost;
    int mainPort;

    int versionAccountInfo = 0;

    friend QDataStream &operator <<(QDataStream &s, AccountInfo &a);
    friend QDataStream &operator >>(QDataStream &s, AccountInfo &a);
};

#endif // ACCOUNTINFO_H
