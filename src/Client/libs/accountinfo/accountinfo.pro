#-------------------------------------------------
#
# Project created by QtCreator 2016-03-21T13:17:51
#
#-------------------------------------------------

QT       -= gui

TARGET = accountinfo
TEMPLATE = lib

include($$PWD/../../path.pri)

DESTDIR = $$BIN_PATH/libs

DEFINES += ACCOUNTINFO_LIBRARY

SOURCES += accountinfo.cpp

HEADERS += accountinfo.h

DISTFILES += \
    accountinfo.pri
