#ifndef MKEY_H
#define MKEY_H

#include <QtCore/qglobal.h>
#include <QtCore/QString>
#include <QtCore/QMap>

#if defined(MKEY_LIBRARY)
#  define MKEYSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MKEYSHARED_EXPORT Q_DECL_IMPORT
#endif

class MKEYSHARED_EXPORT Mkey
{
private:
    QMap <int, QString> keyMap;
public:
    Mkey();
    QString getString(int i);
    QString getString(int i, Qt::KeyboardModifiers m);
    bool isPresent(int i);
};

#endif // MKEY_H
