#-------------------------------------------------
#
# Project created by QtCreator 2016-02-14T23:30:08
#
#-------------------------------------------------

QT       -= gui

TARGET = mkey
TEMPLATE = lib

include($$PWD/../../path.pri)

DESTDIR = $$BIN_PATH/libs

DEFINES += MKEY_LIBRARY

SOURCES += mkey.cpp

HEADERS += mkey.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    mkey.pri
