#include "mkey.h"

Mkey::Mkey()
{
    keyMap.insert(Qt::Key_0, "0");
    keyMap.insert(Qt::Key_1, "1");
    keyMap.insert(Qt::Key_2, "2");
    keyMap.insert(Qt::Key_3, "3");
    keyMap.insert(Qt::Key_4, "4");
    keyMap.insert(Qt::Key_5, "5");
    keyMap.insert(Qt::Key_6, "6");
    keyMap.insert(Qt::Key_7, "7");
    keyMap.insert(Qt::Key_8, "8");
    keyMap.insert(Qt::Key_9, "9");
    keyMap.insert(Qt::Key_A, "A");
    keyMap.insert(Qt::Key_B, "B");
    keyMap.insert(Qt::Key_C, "C");
    keyMap.insert(Qt::Key_D, "D");
    keyMap.insert(Qt::Key_E, "E");
    keyMap.insert(Qt::Key_F, "F");
    keyMap.insert(Qt::Key_G, "G");
    keyMap.insert(Qt::Key_H, "H");
    keyMap.insert(Qt::Key_I, "I");
    keyMap.insert(Qt::Key_K, "K");
    keyMap.insert(Qt::Key_L, "L");
    keyMap.insert(Qt::Key_M, "M");
    keyMap.insert(Qt::Key_N, "N");
    keyMap.insert(Qt::Key_O, "O");
    keyMap.insert(Qt::Key_P, "P");
    keyMap.insert(Qt::Key_Q, "Q");
    keyMap.insert(Qt::Key_R, "R");
    keyMap.insert(Qt::Key_S, "S");
    keyMap.insert(Qt::Key_T, "T");
    keyMap.insert(Qt::Key_U, "U");
    keyMap.insert(Qt::Key_V, "V");
    keyMap.insert(Qt::Key_W, "W");
    keyMap.insert(Qt::Key_X, "X");
    keyMap.insert(Qt::Key_Y, "Y");
    keyMap.insert(Qt::Key_Z, "Z");
    keyMap.insert(Qt::Key_Up, "Up");
    keyMap.insert(Qt::Key_Down, "Down");
    keyMap.insert(Qt::Key_Left, "Left");
    keyMap.insert(Qt::Key_Right, "Right");
    keyMap.insert(Qt::Key_F1, "F1");
    keyMap.insert(Qt::Key_F2, "F2");
    keyMap.insert(Qt::Key_F3, "F3");
    keyMap.insert(Qt::Key_F4, "F4");
    keyMap.insert(Qt::Key_F5, "F5");
    keyMap.insert(Qt::Key_F6, "F6");
    keyMap.insert(Qt::Key_F7, "F7");
    keyMap.insert(Qt::Key_F8, "F8");
    keyMap.insert(Qt::Key_F9, "F9");
    keyMap.insert(Qt::Key_F10, "F10");
    keyMap.insert(Qt::Key_F11, "F11");
    keyMap.insert(Qt::Key_F12, "F12");
    keyMap.insert(Qt::Key_Backslash, "\\");
    keyMap.insert(Qt::Key_Backspace, "Backspace");
    keyMap.insert(Qt::Key_Comma, ",");
    keyMap.insert(Qt::Key_Delete, "Del");
    keyMap.insert(Qt::Key_End, "End");
    keyMap.insert(Qt::Key_Enter, "Enter");
    keyMap.insert(Qt::Key_Escape, "Esc");
    keyMap.insert(Qt::Key_Home, "Home");
    keyMap.insert(Qt::Key_Minus, "-");
    keyMap.insert(Qt::Key_PageDown, "PgDn");
    keyMap.insert(Qt::Key_PageUp, "PgUp");
    keyMap.insert(Qt::Key_Plus, "+");
    keyMap.insert(Qt::Key_Return, "Return");
    keyMap.insert(Qt::Key_Slash, "/");
    keyMap.insert(Qt::Key_Space, "' '");
    keyMap.insert(Qt::Key_VolumeDown, "Vdown");
    keyMap.insert(Qt::Key_VolumeMute, "Vup");
    keyMap.insert(Qt::Key_VolumeUp, "Mute");
    keyMap.insert(Qt::Key_Period, ".");
    keyMap.insert(Qt::Key_Apostrophe, "\"");
    keyMap.insert(Qt::Key_QuoteLeft, "~");
}

QString Mkey::getString(int i)
{
    return keyMap.value(i);
}

bool Mkey::isPresent(int i)
{
    return keyMap.contains(i);
}

QString Mkey::getString(int i, Qt::KeyboardModifiers m)
{
    if (m & Qt::AltModifier
            && m & Qt::ControlModifier
            && m & Qt::ShiftModifier)
        return "Ctrl+Alt+Shift+" + keyMap.value(i);
    if (m & Qt::AltModifier
            && m & Qt::ControlModifier)
        return "Ctrl+Alt+" + keyMap.value(i);
    if (m & (Qt::ShiftModifier
            && m & Qt::AltModifier))
        return "Alt+Shift+" + keyMap.value(i);
    if (m & Qt::ControlModifier
            && m & Qt::ShiftModifier)
        return "Ctrl+Shift+" + keyMap.value(i);
    if (m & Qt::ShiftModifier)
        return "Shift+" + keyMap.value(i);
    if (m & Qt::ControlModifier)
        return "Ctrl+" + keyMap.value(i);
    if (m & Qt::AltModifier)
        return "Alt+" + keyMap.value(i);
    return keyMap.value(i);
}
