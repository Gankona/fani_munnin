#include "mpulldownwidget.h"


MPullDownWidget::MPullDownWidget(QString title, QWidget *parent, int size)
    : QFrame(parent), size(size)
{
    isPullDownStatus = false;
    titleLabel = new QLabel(title, this);
    openButton = new QLabel("v", this);
    content = new QFrame(this);
    setPullDown(isPullDownStatus);
}

MPullDownWidget::MPullDownWidget(QWidget *parent, int size, bool isPullDownStatus)
    : QFrame(parent), size(size), isPullDownStatus(isPullDownStatus)
{
    titleLabel = new QLabel(this);
    openButton = new QLabel("v", this);
    content = new QFrame(this);
    setPullDown(isPullDownStatus);
}

void MPullDownWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if (this->childAt(e->pos()) == titleLabel
            || this->childAt(e->pos()) == openButton)
        this->setPullDown(!isPullDownStatus);
}

void MPullDownWidget::resizeEvent(QResizeEvent *e)
{
    QSize size;
    e == nullptr ? size = QSize(e->size().width(), this->size)
                 : size = e->size();
    titleLabel->setFixedSize(size.width() - this->size, this->size);
    openButton->setFixedSize(this->size, this->size);
    titleLabel->move(0, 0);
    openButton->move(size.width() - this->size, 0);

    content->setVisible(isPullDownStatus);
    if (isPullDownStatus){
        content->setFixedSize(size.width(), size.height() - this->size);
        content->move(0, this->size);
    }
    else
        this->setFixedSize(size.width(), this->size);
}

void MPullDownWidget::setTitle(QString title)
{
    titleLabel->setText(title);
}

QString MPullDownWidget::title()
{
    return titleLabel->text();
}

void MPullDownWidget::setPullDown(bool isPullDownStatus)
{
    this->isPullDownStatus = isPullDownStatus;
    resizeEvent(nullptr);
    emit changePullDownStatus(isPullDownStatus);
}

bool MPullDownWidget::isPullDown()
{
    return isPullDownStatus;
}
