#ifndef MPULLDOWNWIDGET_H
#define MPULLDOWNWIDGET_H

#include <QtCore/qglobal.h>

#include <QtGui/QResizeEvent>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>

#if defined(MPULLDOWNWIDGET_LIBRARY)
#  define MPULLDOWNWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MPULLDOWNWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MPULLDOWNWIDGETSHARED_EXPORT MPullDownWidget : public QFrame
{
    Q_OBJECT
private:
    QLabel *titleLabel;
    QLabel *openButton;
    int size;
    bool isPullDownStatus;

protected:
    void mouseReleaseEvent(QMouseEvent *e);
    void resizeEvent(QResizeEvent *e);

public:
    explicit MPullDownWidget(QWidget *parent = nullptr, int size = 25, bool isPullDownStatus = false);
    MPullDownWidget(QString title, QWidget *parent = nullptr, int size = 25);

    QFrame *content;

    void setTitle(QString title);
    void setSize(int size);
    void setPullDown(bool isPullDownStatus);
    bool isPullDown();
    QString title();

signals:
    void changePullDownStatus(bool);
};

#endif // MPULLDOWNWIDGET_H
