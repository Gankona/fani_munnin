#-------------------------------------------------
#
# Project created by QtCreator 2016-02-26T00:20:30
#
#-------------------------------------------------

QT       += widgets

TARGET = mpulldownwidget
TEMPLATE = lib

include($$PWD/../../path.pri)
DESTDIR = $$BIN_PATH/libs

DEFINES += MPULLDOWNWIDGET_LIBRARY

SOURCES += mpulldownwidget.cpp

HEADERS += mpulldownwidget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    mpulldownwidget.pri
