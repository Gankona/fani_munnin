include($$PWD/../../path.pri)
include($$SRC_PATH/libs/mservice/mservice.pri)

isEmpty(marginwidget.pri){
    LIBS += -L$$BIN_PATH/libs/ -lmarginwidget
    INCLUDEPATH += $$SRC_PATH/libs/mmarginwidget
    DEPENDPATH  += $$SRC_PATH/libs/mmarginwidget
}
