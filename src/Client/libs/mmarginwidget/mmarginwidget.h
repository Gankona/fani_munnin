#ifndef MMARGINWIDGET_H
#define MMARGINWIDGET_H

#include "mservice.h"

#include <QtCore/qglobal.h>
#include <QtGui/QResizeEvent>
#include <QtGui/QShowEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QWidget>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>

#if defined(MMARGINWIDGET_LIBRARY)
#  define MMARGINWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MMARGINWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MMARGINWIDGETSHARED_EXPORT Mmarginwidget : public QFrame
{
private:
    float factor;
    SideEnum side;

    void resizeEvent(QResizeEvent *e);
    void showEvent(QShowEvent*);

public:
    Mmarginwidget(QWidget *parent = nullptr,
                  SideEnum side = SideEnum::LeftRight,
                  float factor = 1.4);
    QFrame *content;

    void setMarginSide(SideEnum side);
    void setFactor(float factor);
    float getFactor();
    void sideUpdate();

    virtual void resizeContent() = 0;
};

#endif // MMARGINWIDGET_H
