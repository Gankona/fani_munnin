#include "mmarginwidget.h"

Mmarginwidget::Mmarginwidget(QWidget *parent, SideEnum side, float factor)
    : QFrame(parent), side(side), factor(factor)
{
    content = new QFrame(this);
}

void Mmarginwidget::setMarginSide(SideEnum side)
{
    this->side = side;
    this->setFixedSize(size());
}

void Mmarginwidget::setFactor(float factor)
{
    this->factor = factor;
    this->setFixedSize(size());
}

void Mmarginwidget::showEvent(QShowEvent *)
{
    resizeContent();
}

float Mmarginwidget::getFactor()
{
    return factor;
}

void Mmarginwidget::resizeEvent(QResizeEvent *e)
{
    if (side == SideEnum::NoSide){
        content->setFixedSize(e->size());
        content->move(0, 0);
        return;
    }
    
    int sideNum = MService::getSideEnumNumber(side);
    bool isLeft  = sideNum%2;
    sideNum /= 2;
    bool isTop = sideNum%2;
    sideNum /= 2;
    bool isRight   = sideNum%2;
    sideNum /= 2;
    bool isBottom = sideNum;
    
    QSize oldContentSize = content->size();
    if (content->children().length() == 1){
        if (factor > 2)
            factor = 2;
        if (factor < 1)
            factor = 1;
        isLeft || isRight ? content->setFixedWidth(e->size().width()*0.5*factor)
                          : content->setFixedWidth(e->size().width());
        isTop || isBottom ? content->setFixedHeight(e->size().height()*0.5*factor)
                          : content->setFixedHeight(e->size().height());
        static_cast<QWidget*>(content->children().at(0))->setFixedSize(content->size());
    }

    int x(0);
    int y(0);

    switch (side){
    case SideEnum::Left:
    case SideEnum::LeftTop:
    case SideEnum::LeftBottom:
    case SideEnum::UnRight:
        x = e->size().width() - content->width(); break;
    case SideEnum::UnBottom:
    case SideEnum::UnTop:
    case SideEnum::LeftRight:
        x = (e->size().width() - content->width())/2; break;
    default:;
    }

    switch (side){
    case SideEnum::Top:
    case SideEnum::LeftTop:
    case SideEnum::RightTop:
    case SideEnum::UnBottom:
        y = e->size().height() - content->height(); break;
    case SideEnum::UnLeft:
    case SideEnum::UnRight:
    case SideEnum::TopBottom:
        y = (e->size().height() - content->height())/2; break;
    default:;
    }

    content->move(x, y);
    if (content->size() != oldContentSize)
        resizeContent();
}
