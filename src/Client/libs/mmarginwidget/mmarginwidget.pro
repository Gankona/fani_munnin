#-------------------------------------------------
#
# Project created by QtCreator 2016-02-04T20:14:20
#
#-------------------------------------------------

QT       += widgets

QT       -= gui

TARGET = marginwidget
TEMPLATE = lib

include($$PWD/../../path.pri)
include($$SRC_PATH/libs/mservice/mservice.pri)

DESTDIR = $$BIN_PATH/libs

DEFINES += MMARGINWIDGET_LIBRARY

SOURCES += mmarginwidget.cpp

HEADERS += mmarginwidget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    marginwidget.pri
