#include "mcontentwidget.h"

MContentWidget::MContentWidget(QWidget *parent, QString name, QString iconPath)
    : QWidget(parent)
{
    titleLabel = MService::alignCenterLabel(this, name);
    iconLabel = MService::squarePictureLabel(this, iconPath);
    sideBar = new MSidePanelBar(this);
    contentWidget = new QWidget(this);
}

void MContentWidget::resizeEvent(QResizeEvent *e)
{
    titleLabel->setFixedWidth(e->size().width() - iconLabel->width());
    if (iconLabel->height() != titleLabel->height())
        iconLabel->setFixedHeight(titleLabel->height());

    switch (sideBar->getSide()){
    case SideEnum::Top:
        iconLabel->move(0, 0);
        titleLabel->move(iconLabel->width(), 0);
        sideBar->setFixedSize(e->size().width(), sideBar->getButtonSize().height());
        sideBar->move(0, iconLabel->height());
        contentWidget->setFixedSize(e->size().width(), e->size().height() -
                                    (sideBar->height() + iconLabel->height()));
        contentWidget->move(0, sideBar->height() + iconLabel->height());
        break;
    case SideEnum::Left:
        iconLabel->move(0, 0);
        if (iconLabel->width() != sideBar->width())
            iconLabel->setFixedWidth(sideBar->width());
        titleLabel->move(iconLabel->width(), 0);
        sideBar->setFixedHeight(e->size().height() - iconLabel->height());
        sideBar->move(0, iconLabel->height());
        contentWidget->setFixedSize(e->size().width() - sideBar->width(),
                                    e->size().height() - titleLabel->height());
        contentWidget->move(sideBar->width(), titleLabel->height());
        break;
    case SideEnum::Bottom:
        iconLabel->move(0, 0);
        titleLabel->move(iconLabel->width(), 0);
        sideBar->setFixedSize(e->size().width(), sideBar->getButtonSize().height());
        sideBar->move(0, e->size().height() - iconLabel->height());
        contentWidget->setFixedSize(e->size().width(), e->size().height() -
                                    (sideBar->height() + iconLabel->height()));
        contentWidget->move(0, iconLabel->height());
        break;
    case SideEnum::Right:
        iconLabel->move(titleLabel->width(), 0);
        if (iconLabel->width() != sideBar->width())
            iconLabel->setFixedWidth(sideBar->width());
        titleLabel->move(0, 0);
        sideBar->setFixedSize(e->size().width(), sideBar->getButtonSize().height());
        sideBar->move(e->size().width() - sideBar->width(), iconLabel->height());
        contentWidget->setFixedSize(e->size().width() - sideBar->width(),
                                    e->size().height() - titleLabel->height());
        contentWidget->move(0, titleLabel->height());
        break;
    default:
        sideBar->setSide(SideEnum::Left);
        this->setFixedSize(this->size());
    }
    foreach (QObject *o, contentWidget->children())
        static_cast<QWidget*>(o)->setFixedSize(contentWidget->size());
}
