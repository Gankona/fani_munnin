#-------------------------------------------------
#
# Project created by QtCreator 2016-01-26T21:49:51
#
#-------------------------------------------------

QT       += widgets

TARGET = mcontentwidget
TEMPLATE = lib

include($$PWD/../../path.pri)
DESTDIR = $$BIN_PATH/libs

include($$SRC_PATH/libs/msidepanelbar/msidepanelbar.pri)
include($$SRC_PATH/libs//mservice/mservice.pri)

DEFINES += MCONTENTWIDGET_LIBRARY

SOURCES += mcontentwidget.cpp

HEADERS += mcontentwidget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    mcontentwidget.pri
