#ifndef MCONTENTWIDGET_H
#define MCONTENTWIDGET_H

#include "mservice.h"
#include "msidepanelbar.h"

#include <QtCore/qglobal.h>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

#if defined(MCONTENTWIDGET_LIBRARY)
#  define MCONTENTWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MCONTENTWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MCONTENTWIDGETSHARED_EXPORT MContentWidget : public QWidget
{
private:
    void resizeEvent(QResizeEvent *e);

protected:
    QLabel *titleLabel;
    MSidePanelBar *sideBar;
    QLabel *iconLabel;
    QWidget *contentWidget;

public:
    MContentWidget(QWidget *parent = nullptr, QString name = "", QString iconPath = "");
};

#endif // MCONTENTWIDGET_H
