include($$PWD/../../path.pri)
include($$SRC_PATH/libs/msidepanelbar/msidepanelbar.pri)

isEmpty(mcontentwidget.pri){
    LIBS += -L$$BIN_PATH/libs/ -lmcontentwidget
    INCLUDEPATH += $$SRC_PATH/libs/mcontentwidget
    DEPENDPATH  += $$SRC_PATH/libs/mcontentwidget
}
