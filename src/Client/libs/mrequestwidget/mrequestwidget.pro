#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T12:21:52
#
#-------------------------------------------------

QT       += widgets

TARGET = mrequestwidget
TEMPLATE = lib

include($$PWD/../../path.pri)
DESTDIR = $$BIN_PATH/libs

DEFINES += MREQUESTWIDGET_LIBRARY

SOURCES += mrequestwidget.cpp

HEADERS += mrequestwidget.h

DISTFILES += \
    mrequestwidget.pri
