#ifndef MREQUESTWIDGET_H
#define MREQUESTWIDGET_H

#include <QtCore/qglobal.h>

#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>

#if defined(MREQUESTWIDGET_LIBRARY)
#  define MREQUESTWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MREQUESTWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MREQUESTWIDGETSHARED_EXPORT MRequestWidget : public QFrame
{
    Q_OBJECT
private:
    QLabel *titleLabel;
    QLabel *

public:
    MRequestWidget();
};

#endif // MREQUESTWIDGET_H
