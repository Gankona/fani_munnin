TEMPLATE = subdirs

SUBDIRS += \
    mservice \
    msidepanelbar \
    mopacitywidget \
    mswitcherpos \
    mcontentwidget \
    mmarginwidget \
    mkey \
    mhlistwidget \
    mpulldownwidget \
    accountinfo
