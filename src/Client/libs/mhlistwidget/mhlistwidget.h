#ifndef MHLISTWIDGET_H
#define MHLISTWIDGET_H

#include <QtCore/qglobal.h>
#include <QtGui/QMouseEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFontComboBox>
#include <QDebug>

#if defined(MHLISTWIDGET_LIBRARY)
#  define MHLISTWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MHLISTWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MHLISTWIDGETSHARED_EXPORT MHListWidget : public QFrame
{
private:
    QLabel *prevButton;
    QLabel *nextButton;
    QComboBox *listCBox;
    QFontComboBox *listFBox;
    QSize size;
    bool isFontWidget;

protected:
    void mouseReleaseEvent(QMouseEvent *e);
    void resizeEvent(QResizeEvent *e);

public:
    MHListWidget(QWidget *parent = nullptr,
                 bool isFontWidget = false,
                 QSize size = QSize(150, 25));
    QString getCurrentVar();
    void setCurrentVar(QString var);
    QComboBox* comboBox();
};

#endif // MHLISTWIDGET_H
