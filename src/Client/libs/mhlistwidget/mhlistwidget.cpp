#include "mhlistwidget.h"


MHListWidget::MHListWidget(QWidget *parent, bool isFontWidget, QSize size)
    : QFrame(parent), size(size), isFontWidget(isFontWidget)
{
    prevButton = new QLabel("<-", this);
    nextButton = new QLabel("->", this);
    isFontWidget ? listFBox = new QFontComboBox(this)
                 : listCBox = new QComboBox(this);
    if (isFontWidget)
        this->size += QSize(50, 0);
    this->setFixedSize(size);
    this->show();
}

QString MHListWidget::getCurrentVar()
{
    if (isFontWidget)
        return listFBox->currentText();
    return listCBox->currentText();
}

void MHListWidget::setCurrentVar(QString var)
{
    isFontWidget ? listFBox->setCurrentText(var)
                 : listCBox->setCurrentText(var);
}

QComboBox* MHListWidget::comboBox()
{
    if (isFontWidget)
        return static_cast<QComboBox*>(listFBox);
    return listCBox;
}

void MHListWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->pos().x() > prevButton->x()
            && e->pos().x() < prevButton->x() + prevButton->width()
            && e->pos().y() > prevButton->y()
            && e->pos().y() > prevButton->y() + prevButton->height()){
        if (isFontWidget){
            if (listFBox->currentIndex() != 0)
                listFBox->setCurrentIndex(listFBox->currentIndex()-1);
        }
        else {
            if (listCBox->currentIndex() != 0)
                listCBox->setCurrentIndex(listCBox->currentIndex()-1);
        }
    }
    if (e->pos().x() > nextButton->x()
            && e->pos().x() < nextButton->x() + nextButton->width()
            && e->pos().y() > nextButton->y()
            && e->pos().y() > nextButton->y() + nextButton->height()){
        if (isFontWidget){
            if (listFBox->currentIndex() < listFBox->count()-1)
                listFBox->setCurrentIndex(listFBox->currentIndex()+1);
        }
        else {
            if (listCBox->currentIndex() < listCBox->count()-1)
                listCBox->setCurrentIndex(listCBox->currentIndex()+1);
        }
    }
}

void MHListWidget::resizeEvent(QResizeEvent *e)
{
    QSize size;
    e == nullptr ? size = this->size : size = e->size();

    prevButton->setFixedSize(size.height(), size.height());
    prevButton->move(0, 0);

    nextButton->setFixedSize(prevButton->size());
    nextButton->move(size.width()-size.height(), 0);

    if (isFontWidget){
        listFBox->setFixedSize(size.width() - 2*size.height(), size.height());
        listFBox->move(size.height(), 0);
    }
    else {
        listCBox->setFixedSize(size.width() - 2*size.height(), size.height());
        listCBox->move(size.height(), 0);
    }
}
