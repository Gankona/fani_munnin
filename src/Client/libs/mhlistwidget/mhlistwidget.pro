#-------------------------------------------------
#
# Project created by QtCreator 2016-02-18T12:01:42
#
#-------------------------------------------------

QT       += widgets

TARGET = mhlistwidget
TEMPLATE = lib

include($$PWD/../../path.pri)

DESTDIR = $$BIN_PATH/libs

DEFINES += MHLISTWIDGET_LIBRARY

SOURCES += mhlistwidget.cpp

HEADERS += mhlistwidget.h

DISTFILES += \
    mhlistwidget.pri
