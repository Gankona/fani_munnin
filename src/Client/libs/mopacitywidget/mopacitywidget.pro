#-------------------------------------------------
#
# Project created by QtCreator 2016-01-26T21:48:47
#
#-------------------------------------------------

QT       += widgets

TARGET = mopacitywidget
TEMPLATE = lib

include($$PWD/../../path.pri)
DESTDIR = $$BIN_PATH/libs

DEFINES += MOPACITYWIDGET_LIBRARY

SOURCES += mopacitywidget.cpp

HEADERS += mopacitywidget.h

DISTFILES += \
    mopacitywidget.pri
