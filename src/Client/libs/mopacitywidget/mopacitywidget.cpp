#include "mopacitywidget.h"

MOpacityWidget::MOpacityWidget(QWidget *parent, int opacityValue)
    : QWidget(parent), opacityValue(opacityValue)
{
    opacity = new QGraphicsOpacityEffect(this);
    opacity->setOpacity(opacityValue);
    this->setGraphicsEffect(opacity);
    floatEffect = true;
}

void MOpacityWidget::hideEvent(QHideEvent *e)
{
    qDebug() << e << " #$% hide $%^ ";
}

void MOpacityWidget::showEvent(QShowEvent *e)
{
    qDebug() << e << " #$% show $%^ ";
}

bool MOpacityWidget::isSetFloatEffect()
{
    return floatEffect;
}

void MOpacityWidget::setFloatEffect(bool floatEffect)
{
    this->floatEffect = floatEffect;
}

int  MOpacityWidget::getOpacityValue()
{
    return opacityValue;
}

void MOpacityWidget::setUnOpasity()
{
    setOpacityValue(100);
}

void MOpacityWidget::setOpacityValue(int opacity)
{
    opacityValue = opacity;
    this->opacity->setOpacity(opacityValue);
    repaint();
}

//#include "mfloatopasitywidget.h"

//MFloatOpasityWidget::MFloatOpasityWidget(QWidget *parent) : QFrame(parent)
//{
//    opacity = new QGraphicsOpacityEffect(this);
//    opacity->setOpacity(0);
//    this->setGraphicsEffect(opacity);
//}

//void MFloatOpasityWidget::showWidget(QPoint point)
//{
//    qDebug() << point << parent();
//    this->move(point);
//    this->show();
//    this->raise();
//    QPropertyAnimation *animation = new QPropertyAnimation(opacity, "opacity");
//    animation->setDuration(700);
//    animation->setStartValue(0);
//    animation->setEndValue(0.8);
//    animation->start();
//}

//void MOpasityWidget::deleteWidget()
//{
//    QPropertyAnimation *animation = new QPropertyAnimation(opacity, "opacity");
//    animation->setDuration(500);
//    animation->setStartValue(0.8);
//    animation->setEndValue(0);
//    animation->start();

//    QObject::connect(animation, SIGNAL(finished()), this, SLOT(deleteLater()));
//}

