#ifndef MOPACITYWIDGET_H
#define MOPACITYWIDGET_H

#include <QDebug>
#include <QtCore/qglobal.h>
#include <QtCore/QPropertyAnimation>
#include <QtGui/QShowEvent>
#include <QtGui/QHideEvent>
#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtWidgets/QWidget>

#if defined(MOPACITYWIDGET_LIBRARY)
#  define MOPACITYWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MOPACITYWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MOPACITYWIDGETSHARED_EXPORT MOpacityWidget : public QWidget
{
private:
    int opacityValue;
    bool floatEffect;
    QGraphicsOpacityEffect *opacity;

    void hideEvent(QHideEvent *e);
    void showEvent(QShowEvent *e);

public:
    MOpacityWidget(QWidget *parent = nullptr,
                   int opacityValue = 85);

    bool isSetFloatEffect();
    void setFloatEffect(bool floatEffect);

    int  getOpacityValue();
    void setUnOpasity();
    void setOpacityValue(int opacity);
};

#endif // MOPACITYWIDGET_H
