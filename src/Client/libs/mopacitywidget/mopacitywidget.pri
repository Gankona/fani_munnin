include($$PWD/../../path.pri)

isEmpty(mopacitywidget.pri){
    LIBS += -L$$BIN_PATH/libs/ -lmopacitywidget
    INCLUDEPATH += $$SRC_PATH/libs/mopacitywidget
    DEPENDPATH  += $$SRC_PATH/libs/mopacitywidget
}
