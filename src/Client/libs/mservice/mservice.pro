#-------------------------------------------------
#
# Project created by QtCreator 2016-01-26T21:46:33
#
#-------------------------------------------------

CONFIG += c++14

QT       += widgets

TARGET = mservice
TEMPLATE = lib


include($$PWD/../../path.pri)

DESTDIR = $$BIN_PATH/libs

DEFINES += MSERVICE_LIBRARY

SOURCES += mservice.cpp

HEADERS += mservice.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    mservice.pri
