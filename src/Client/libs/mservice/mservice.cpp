#include "mservice.h"

QPushButton* MService::flatSquarePictureButton(QIcon icon, QWidget *parent,
                                        QString toolTip, QSize size)
{
    QPushButton *b = new QPushButton(parent);
    b->setFlat(true);
    b->setIconSize(size);
    b->setFixedSize(size);
    b->setCheckable(true);
    b->setToolTip(toolTip);
    b->setIcon(icon);
    b->setFocusPolicy(Qt::NoFocus);
    return b;
}

QPushButton* MService::flatButton(QWidget *parent, QString text, QSize size)
{
    QPushButton *b = new QPushButton(text, parent);
    b->setFixedSize(size);
    b->setFlat(true);
    b->setFocusPolicy(Qt::NoFocus);
    b->setCheckable(true);
    return b;
}

QLabel* MService::alignCenterLabel(QWidget *parent, QString text, int fixedHeight)
{
    QLabel *l = new QLabel(text, parent);
    l->setAlignment(Qt::AlignCenter);
    l->setFixedHeight(fixedHeight);
    return l;
}

QLabel* MService::squarePictureLabel(QWidget *parent, QString icon, QSize size)
{
    QLabel *l = new QLabel(parent);
    l->setFixedSize(size);
    //l->setPicture(QPixmap(icon, 0, Qt::ImageConversionFlag::ContainsItemBoundingRect));
    return l;
}

int MService::getSideEnumNumber(SideEnum side)
{
    for (int i = 0; i < 16; i++)
        if (i == side)
            return i;
    return 0;
}

QString MService::getStringKey(QKeyEvent *e)
{
    QString modif = "";
    if (e->modifiers() & Qt::ShiftModifier)
        modif += "Shift+";
    if (e->modifiers() & Qt::ControlModifier)
        modif += "Ctrl+";
    if (e->modifiers() & Qt::AltModifier)
        modif += "Alt+";

    switch (e->key()){
    case Qt::Key_0:
        return modif + "0";
    case Qt::Key_1:
        return modif + "1";
    case Qt::Key_2:
        return modif + "2";
    case Qt::Key_3:
        return modif + "3";
    case Qt::Key_4:
        return modif + "4";
    case Qt::Key_5:
        return modif + "5";
    case Qt::Key_6:
        return modif + "6";
    case Qt::Key_7:
        return modif + "7";
    case Qt::Key_8:
        return modif + "8";
    case Qt::Key_9:
        return modif + "9";
    case Qt::Key_A:
        return modif + "A";
    case Qt::Key_Alt:
        return modif + "Alt";
    case Qt::Key_B:
        return modif + "B";
    case Qt::Key_C:
        return modif + "C";
    case Qt::Key_D:
        return modif + "D";
    case Qt::Key_E:
        return modif + "E";
    case Qt::Key_F:
        return modif + "F";
    case Qt::Key_G:
        return modif + "G";
    case Qt::Key_H:
        return modif + "H";
    case Qt::Key_I:
        return modif + "I";
    case Qt::Key_K:
        return modif + "K";
    case Qt::Key_L:
        return modif + "L";
    case Qt::Key_M:
        return modif + "M";
    case Qt::Key_N:
        return modif + "N";
    case Qt::Key_O:
        return modif + "O";
    case Qt::Key_P:
        return modif + "P";
    case Qt::Key_R:
        return modif + "R";
    case Qt::Key_S:
        return modif + "S";
    case Qt::Key_T:
        return modif + "T";
    case Qt::Key_U:
        return modif + "U";
    case Qt::Key_V:
        return modif + "V";
    case Qt::Key_W:
        return modif + "W";
    case Qt::Key_X:
        return modif + "X";
    case Qt::Key_Y:
        return modif + "Y";
    case Qt::Key_Z:
        return modif + "Z";
    case Qt::Key_Up:
        return modif + "Up";
    case Qt::Key_Down:
        return modif + "Down";
    case Qt::Key_Right:
        return modif + "Right";
    case Qt::Key_Left:
        return modif + "Left";
    case Qt::Key_Delete:
        return modif + "Del";
    case Qt::Key_Backspace:
        return modif + "Back";
    case Qt::Key_End:
        return modif + "End";
    case Qt::Key_Enter:
        return modif + "Enter";
    case Qt::Key_Escape:
        return modif + "Esc";
    case Qt::Key_F1:
        return modif + "F1";
    case Qt::Key_F2:
        return modif + "F2";
    case Qt::Key_F3:
        return modif + "F3";
    case Qt::Key_F4:
        return modif + "F4";
    case Qt::Key_F5:
        return modif + "F5";
    case Qt::Key_F6:
        return modif + "F6";
    case Qt::Key_F7:
        return modif + "F7";
    case Qt::Key_F8:
        return modif + "F8";
    case Qt::Key_F9:
        return modif + "F9";
    case Qt::Key_F10:
        return modif + "F10";
    case Qt::Key_F11:
        return modif + "F11";
    case Qt::Key_F12:
        return modif + "F12";
    case Qt::Key_Insert:
        return modif + "Ins";
    case Qt::Key_Minus:
        return modif + "-";
    case Qt::Key_Plus:
        return modif + "+";
    case Qt::Key_PageDown:
        return modif + "PgDn";
    case Qt::Key_PageUp:
        return modif + "PgUp";
    case Qt::Key_Print:
        return modif + "Print";
    case Qt::Key_Spell:
        return modif + "\\";
    case Qt::Key_Slash:
        return modif + "/";
    default: return "unknown";
    }
}
