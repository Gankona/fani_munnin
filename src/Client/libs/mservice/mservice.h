#ifndef MSERVICE_H
#define MSERVICE_H

#include <QDebug>
#include <QtCore/qglobal.h>
#include <QtGui/QPixmap>
#include <QtGui/QKeyEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

#if defined(MSERVICE_LIBRARY)
#  define MSERVICESHARED_EXPORT Q_DECL_EXPORT
#else
#  define MSERVICESHARED_EXPORT Q_DECL_IMPORT
#endif

enum StatusLevel{
    critical = 0,
    denaid = 1,
    inform = 2,
    debug = 3
};

enum SideEnum {
    NoSide = 0,

    Left = 1,
    Top  = 2,
    Right = 4,
    Bottom = 8,

    LeftTop = 3,
    LeftRight = 5,
    RightTop  = 6,
    LeftBottom = 9,
    TopBottom  = 10,
    RightBottom = 12,

    UnBottom = 7,
    UnRight = 11,
    UnTop  = 13,
    UnLeft = 14,

    AllSide = 15
};

class MSERVICESHARED_EXPORT MService
{
public:
    MService(){}
    static QPushButton* flatSquarePictureButton(QIcon icon,
                                                QWidget* parent = nullptr,
                                                QString toolTip = "",
                                                QSize size = QSize(25, 25));

    static QPushButton* flatButton(QWidget* parent = nullptr,
                                   QString text = "",
                                   QSize size = QSize(75, 25));

    static QLabel* alignCenterLabel(QWidget *parent = nullptr,
                                    QString text = "",
                                    int fixedHeight = 25);

    static QLabel* squarePictureLabel(QWidget *parent = nullptr,
                                      QString icon = "",
                                      QSize size = QSize(25, 25));

    static int getSideEnumNumber(SideEnum side);

    static QString getStringKey(QKeyEvent *e);
};

#endif // MSERVICE_H
