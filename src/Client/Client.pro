TEMPLATE = subdirs

SUBDIRS += libs \
           client \
           modules \
           plugins \
           addons \
           styles

DISTFILES += $${PWD}/path.pri
