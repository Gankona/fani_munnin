#-------------------------------------------------
#
# Project created by QtCreator 2016-01-24T15:31:07
#
#-------------------------------------------------
CONFIG += c++14

QT += widgets
QT += network

TARGET = base_fm_munnin
TEMPLATE = lib
CONFIG += plugin

include($$PWD/../../path.pri)
DESTDIR += $$BIN_PATH/modules

SOURCES += \
    desktop_interface/view.cpp \
    base_fm_munnin.cpp \
    control/server.cpp \
    desktop_interface/login/registrationwidget.cpp \
    desktop_interface/login/infowidget.cpp \
    desktop_interface/login/editwidget.cpp \
    desktop_interface/loginwidget.cpp \
    desktop_interface/login/autorizationwidget.cpp \
    control/accountinfo.cpp \
    control/message.cpp

HEADERS += \
    $$SRC_PATH/modules/basemoduleinterface.h \
    desktop_interface/view.h \
    base_fm_munnin.h \
    control/server.h \
    desktop_interface/login/registrationwidget.h \
    desktop_interface/login/infowidget.h \
    desktop_interface/login/editwidget.h \
    desktop_interface/loginwidget.h \
    desktop_interface/login/autorizationwidget.h \
    control/accountinfo.h \
    control/message.h

include($$SRC_PATH/libs/mservice/mservice.pri)
include($$SRC_PATH/libs/msidepanelbar/msidepanelbar.pri)
include($$SRC_PATH/libs/mopacitywidget/mopacitywidget.pri)
include($$SRC_PATH/libs/mpulldownwidget/mpulldownwidget.pri)
include($$SRC_PATH/libs/mswitcherpos/mswitcherpos.pri)

SOURCES += desktop_interface/upwidget.cpp \
    desktop_interface/menuwidget.cpp \
    desktop_interface/homewidget.cpp

HEADERS += desktop_interface/upwidget.h \
    desktop_interface/menuwidget.h \
    desktop_interface/homewidget.h

INCLUDEPATH += $$SRC_PATH/client
INCLUDEPATH += $$SRC_PATH/modules

RESOURCES += \
    resource.qrc
