#ifndef BASE_FM_MUNNIN_H
#define BASE_FM_MUNNIN_H

#ifndef Q_OS_WIN32
#include "desktop_interface/view.h"
#endif
#include "basemoduleinterface.h"
#include "control/server.h"
#include "control/accountinfo.h"
#include "control/messagemanager.h"

#include <QtCore/QThread>
#include <QtCore/QDateTime>
#include <QtCore/QSettings>
#include <QDebug>

class ModuleList {
public:
    BaseModuleInterface* module;
    int position;
    ModuleList(BaseModuleInterface* module, int position)
        : module(module), position(position) {}
};

class Base_fm_munnin : public QObject
{
    Q_OBJECT

private:
    QSettings setting;
    View *view;
    Server *server;
    int currentWidget;
    QThread *serverThread;
    QList <ModuleList*> baseList;

public:
    Base_fm_munnin();
    void startFaniMunnin();
    void findModuls();
    void findAddons();

    QMap <QDateTime, QMap <int, QMap <QString, QString>>> statusList;

public slots:
    void slotSetModule(int i);
    void slotSetHome();
    void slotCloseApp();
    void slotAcceptSetting();
    void slotCatchLogin(Flags flag, Message msg);
    void openEditAccount();
};

#endif // BASE_FM_MUNNIN_H
