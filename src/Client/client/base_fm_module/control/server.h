#ifndef SERVER_H
#define SERVER_H

#include "../../fm_namespace.h"
#include "../desktop_interface/loginwidget.h"

#include <QtCore/QDir>
#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>
#include <QtNetwork/QTcpSocket>

#include <QDebug>

class Server : public QObject
{
    Q_OBJECT
public:
    Server(QObject *parent = nullptr);

private:
    QTcpSocket *socket;
    QDataStream stream;
    QTimer *timer;
    QSettings setting;
    AccountInfo tempAccountInfo;
    QDir dir;
    QMap <QString, bool> profileMap;
    bool isConnected;

    void readProfiles();
    void writeProfiles();
    void writeLoginProfile(bool loginStatus = false);
    void tryLocalLogin(QString login, QString password);

private slots:
    void connected();
    void readyRead();
    void testTimer();

public slots:
    void setLogin(QString login, QString password);
    void setRegistration(AccountInfo *account);
    void updateServerConnected(QString host, int port);
    void logout();
};

#endif // SERVER_H
