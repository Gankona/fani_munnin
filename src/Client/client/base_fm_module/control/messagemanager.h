#ifndef MESSAGEMANAGER_H
#define MESSAGEMANAGER_H

#include "../../fm_namespace.h"

#include <QtCore/QFile>
#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QMultiMap>
#include <QtCore/QDataStream>
#include <QDebug>

using namespace FM::Flags;
using namespace FM::Message;

//Q_DECLARE_METATYPE();

class MessageManager : public QObject
{
    Q_OBJECT
    Q_ENUM(Flags)
    Q_ENUM(Message)

private:
    QDataStream stream;
    QFile *file;
    QTimer *timer;
    QMap <QDateTime, QMap <int, QMap <QString, QString>>> messageList;

public:
    explicit MessageManager(QObject *parent = 0);

    void setFlagMsg(Message msg = Message::unknownError,
                    QString nameModule = "FM_Base",
                    Flags spaceFlag = Flags::showInMessageManagerBar);

    void setFlagMsg(Message msg = Message::unknownError,
                    Flags flag = Flags::unknown,
                    Flags spaceFlag = Flags::showInMessageManagerBar);

    void setStringMsg(QString msg,
                      QString nameModule = "FM_Base",
                      Flags spaceFlag = Flags::showInMessageManagerBar,
                      Flags levelFlag = Flags::information);

public slots:
    void saveMsgList();

signals:
    //void sendMsg(QString moduleName, QString text, FM::Flags spaceFlag, FM::Flags levelFlag, QDateTime d);
    void sendMsg(QString, QString, Flags, Flags, QDateTime);
    void sendFlag(Flags, Message);
};

#endif // MESSAGEMANAGER_H
