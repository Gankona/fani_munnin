#include "server.h"
#include "base_fm_module/control/messagemanager.h"

extern AccountInfo accountInfo;
extern MessageManager message;

Server::Server(QObject *parent) : QObject(parent), setting()
{
    socket = new QTcpSocket(this);
    QObject::connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

    timer = new QTimer(this);
    timer->setInterval(30000);
    timer->start();

    this->updateServerConnected("127.0.0.1", 32802);
    readProfiles();
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(testTimer()));
}

void Server::connected()
{
    qDebug() << "connected to server";
    stream.setDevice(socket);
    qDebug() << "yes DataStream complete";
    isConnected = true;
}

void Server::readyRead()
{
    qDebug() << "Server::readyRead()";
    int flag_i;
    int msg_i;
    stream >> flag_i >> msg_i;
    qDebug() << flag_i << msg_i;
    Flags flag = Flags(flag_i);
    Message msg = Message(msg_i);
    switch (flag_i){

    case Flags::testEmail:
    case Flags::testLogin:
        message.setFlagMsg(msg, flag, Flags::showInDestination);
        break;

    case Flags::ping:
        break;

    case Flags::registration:{
        qDebug() << "Server::readyRead()" << "reg";
        if (msg == Message::successfulRegistraton){
            //тут прописываем если успешно зарегистрировались
            message.setFlagMsg(msg, Flags::registration, Flags::showInMessageManagerBox);
            writeLoginProfile();
            dir.mkdir(tempAccountInfo.login);
            setLogin(tempAccountInfo.login, tempAccountInfo.password);
        }
        else
            message.setFlagMsg(msg, Flags::registration, Flags::showInDestination);
        qDebug() << "ssfss";
        break;
    }

    case Flags::login:{
        qDebug() << "Server::readyRead()" << "login";
        if (msg == Message::successfulLogin){
            //если авторизация успешна
            QString login;
            QDateTime lastTimeUpdateLogin;
            AccountInfo tempAccountLocal;
            stream >> login >> lastTimeUpdateLogin;
            QFile file(dir.absolutePath()+"/"+login+"/"+login+".profile");
            if (file.open(QIODevice::ReadOnly)){
                QDataStream streamLocal;
                streamLocal.setDevice(&file);
                streamLocal >> tempAccountLocal;
                if (tempAccountLocal.lastUpdate < lastTimeUpdateLogin){
                    //отправка заявки на сервер про апдейт профайла
                    stream << int(Flags::getProfile) << login;
                    qDebug() << "Server::readyRead()" << "login2";
                    return;
                }
            }
            else {
                stream << int(Flags::getProfile) << login;
                qDebug() << "Server::readyRead()" << "loginSendToProfile";
                return;
            }
            foreach (QString l, profileMap.keys())
                l == login ? profileMap.insert(login, true)
                           : profileMap.insert(login, false);
            writeProfiles();
            qDebug() << "F Login";
            qDebug() << "accountInfo" << accountInfo.login << accountInfo.password << accountInfo.isNoName;
            qDebug() << "tempAccountLocal" << tempAccountLocal.login << tempAccountLocal.password
                     << tempAccountLocal.isNoName;
            accountInfo = tempAccountLocal;
            qDebug() << "accountInfo" << accountInfo.login << accountInfo.password << accountInfo.isNoName;
            qDebug() << "tempAccountLocal" << tempAccountLocal.login << tempAccountLocal.password
                     << tempAccountLocal.isNoName;
            qDebug() << "case Flags::login after saveaccount" << msg;
            message.setFlagMsg(msg, Flags::login, Flags::showInMessageManagerBox);
        }
        else
            message.setFlagMsg(msg, Flags::login, Flags::showInDestination);
        qDebug() << "Server::readyRead()" << "login3";
        break;
    }

    case Flags::getProfile:{
        qDebug() << "Server::readyRead()" << "getprofile";
        //тут мы получаем профайл и его обрабатываем и сохраняем
        stream >> tempAccountInfo;
        qDebug() << "tempAccountLocal" << tempAccountInfo.login << tempAccountInfo.password
                 << tempAccountInfo.isNoName;
        writeLoginProfile();
        foreach (QString l, profileMap.keys())
            l == tempAccountInfo.login ? profileMap.insert(l, true)
                                        : profileMap.insert(l, false);
        writeLoginProfile(true);
        writeProfiles();
        qDebug() << "F getProfile";
        qDebug() << "accountInfo" << accountInfo.login << accountInfo.password << accountInfo.isNoName;
        qDebug() << "tempAccountLocal" << tempAccountInfo.login << tempAccountInfo.password
                 << tempAccountInfo.isNoName;
        accountInfo = tempAccountInfo;
        qDebug() << "accountInfo" << accountInfo.login << accountInfo.password << accountInfo.isNoName;
        qDebug() << "tempAccountLocal" << tempAccountInfo.login << tempAccountInfo.password
                 << tempAccountInfo.isNoName;
        message.setFlagMsg(Message::successfulLogin, Flags::login, Flags::showInMessageManagerBox);
        qDebug() << "Server::readyRead()" << "getprofile2";
        break;
    }

    }
}

void Server::updateServerConnected(QString host, int port)
{
    socket->connectToHost(host, port);
}

void Server::testTimer()
{
    qDebug() << "timer Server";
}

void Server::readProfiles()
{
    qDebug() << "readProfile";
    dir = QDir::home();
    if (!dir.cd(".fm")){
        dir.mkdir(".fm");
        if (!dir.cd(".fm")){
            message.setFlagMsg(Message::cannotOpenDirectory,
                               Flags::critical,
                               Flags::showInMessageManagerBox);
            qDebug() << "return";
            return;
        }
    }
    setting.setValue("Path/dataPath", dir.absolutePath());
    qDebug() << dir;
    QFile file(dir.absoluteFilePath("users.profiles"));
    QDataStream stream;
    if (file.open(QIODevice::ReadOnly)){
        stream.setDevice(&file);
        QString s;
        int i;
        bool b;
        stream >> i;
        for (int j = 0; j < i; j++){
            stream >> s >> b;
            profileMap.insert(s, b);
        }
        file.close();
        qDebug() << profileMap;
        foreach (QString login, profileMap.keys())
            if (profileMap.value(login)){
                QFile file(dir.absolutePath()+"/"+login+"/"+login+".profile");
                file.open(QIODevice::ReadOnly);
                QDataStream streamLocal;
                streamLocal.setDevice(&file);
                AccountInfo tempAccountLocal;
                streamLocal >> tempAccountLocal;
                accountInfo = tempAccountLocal;
                message.setFlagMsg(Message::successfulLogin, Flags::login, Flags::showInMessageManagerBar);
                //return;
            }
        //logout();
    }
    else {
        file.open(QIODevice::WriteOnly);
        profileMap.insert(accountInfo.login, true);
        stream.setDevice(&file);
        stream << profileMap;
        file.close();
        writeLoginProfile(true);
        readProfiles();
    }
    qDebug() << "readProfile end";
}

void Server::writeLoginProfile(bool loginStatus)
{
    qDebug() << "writeLoginProfile";
    QDir tempDir = dir;
    qDebug() << tempDir << dir;
    qDebug() << tempDir.cd(tempAccountInfo.login);
    qDebug() << tempDir;
    qDebug() << tempDir.absoluteFilePath(tempAccountInfo.login+".profile");
    if (!tempDir.cd(tempAccountInfo.login)){
        tempDir.mkdir(tempAccountInfo.login);
        tempDir.cd(tempAccountInfo.login);
    }
    QFile file(tempDir.absoluteFilePath(tempAccountInfo.login+".profile"));
    qDebug() << "Server::writeLoginProfile()" << file.open(QIODevice::WriteOnly);
    qDebug() << tempAccountInfo.login << tempAccountInfo.password;
    QDataStream stream;
    stream.setDevice(&file);
    stream << tempAccountInfo;
    file.close();
    profileMap.insert(tempAccountInfo.login, loginStatus);
    writeProfiles();
    qDebug() << file.fileName();
    qDebug() << profileMap;
    qDebug() << "writeLoginProfile end";
}

void Server::writeProfiles()
{
    qDebug() << "writeProfile";
    QFile file(dir.absoluteFilePath("users.profiles"));
    file.open(QIODevice::WriteOnly);
    QDataStream stream;
    stream.setDevice(&file);
    stream << profileMap.keys().length();
    foreach (QString s, profileMap.keys())
        stream << s << profileMap.value(s);
    file.close();
    qDebug() << "writeProfile end";
}

void Server::tryLocalLogin(QString login, QString password)
{
    qDebug() << "tryLocalLogin" << login << password;
    foreach (QString key, profileMap.keys())
        if (key == login){
            QDir currentDir = dir;
            if (currentDir.cd(login)){
                QFile file(currentDir.absoluteFilePath(login+".profile"));
                if (file.open(QIODevice::ReadOnly)){
                    AccountInfo tempAccountTryLogin;
                    QDataStream stream;
                    stream.setDevice(&file);
                    stream >> tempAccountTryLogin;
                    file.close();
                    if (login == tempAccountTryLogin.login
                            && password == tempAccountTryLogin.password){
                        accountInfo = tempAccountTryLogin;
                        message.setFlagMsg(Message::successfulLogin,
                                           Flags::login,
                                           Flags::showInMessageManagerBox);
                        foreach (QString l, profileMap.keys())
                            l == login ? profileMap.insert(login, true)
                                       : profileMap.insert(login, false);
                        writeProfiles();
                    }
                }
            }
            else {
                currentDir.mkdir(login);
                tryLocalLogin(login, password);
            }
            return;
        }
    message.setFlagMsg(Message::accountNotPresent, Flags::warning, Flags::showInDestination);
}

void Server::setLogin(QString login, QString password)
{
    if (isConnected){
        stream << int(Flags::login) << login << password;
    }
    else {
        tryLocalLogin(login, password);
    }
    qDebug() << "Login Server::setLogin" << login << password << isConnected;
}

void Server::setRegistration(AccountInfo *account)
{
    if (isConnected){
        stream << int(Flags::registration) << *account;
        qDebug() << "setReg";
        tempAccountInfo = *account;
        qDebug() << "Setreg << tempAcInfo" << tempAccountInfo.login << account->login
                 << tempAccountInfo.password << account->password << tempAccountInfo.isNoName
                 << account->isNoName;
    }
    else {
        delete account;
        message.setFlagMsg(Message::serverNotFound, Flags::registration);
    }
    qDebug() << "Server::setRegistration(AccountInfo *account)";
    qDebug() << &account;
}

void Server::logout()
{
    AccountInfo account;
    profileMap.insert(account.login, true);
    foreach (QString l, profileMap.keys())
        profileMap.insert(l, l == account.login);
    writeProfiles();
    accountInfo = account;
    message.setFlagMsg(Message::successfulLogout, Flags::logout, Flags::showInMessageManagerBar);
    qDebug() << "logout";
}
