#include "messagemanager.h"

MessageManager::MessageManager(QObject *parent) : QObject(parent)
{
    qRegisterMetaType <Flags> ("Flags");
    qRegisterMetaType <Message> ("Message");

    messageList.clear();
    file = new QFile("history.fmlog", this);
    qDebug() << "MessageManager create1";
    timer = new QTimer(this);
    timer->setInterval(1000);
    timer->start();

    file->open(QIODevice::ReadOnly);
    stream.setDevice(file);
    stream >> messageList;
    file->close();
    qDebug() << "MessageManager create";

    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(saveMsgList()));


    qDebug() << Flags::login << "Flags::login";
    qDebug() << Flags::serverNotFound << "Flags::serverNotFound";
    qDebug() << Flags::registration << "Flags::registration";

    qDebug() << Message::accountNotPresent << "Message::accountNotPresent";
    qDebug() << Message::cannotOpenDirectory << "Message::cannotOpenDirectory";
    qDebug() << Message::connectToServer << "Message::connectToServer";
    qDebug() << Message::emailFree << "Message::emailFree";
    qDebug() << Message::emailIncorrect << "Message::emailIncorrect";
    qDebug() << Message::emailNotPresent << "Message::emailNotPresent";
    qDebug() << Message::emailPresent << "Message::emailPresent";
    qDebug() << Message::insecurePassword << "Message::insecurePassword";
    qDebug() << Message::loginFree << "Message::loginFree";
    qDebug() << Message::loginPresent << "Message::loginPresent";
    qDebug() << Message::passwordDoNotMatch << "Message::passwordDoNotMatch";
    qDebug() << Message::passwordIncorrect << "Message::passwordIncorrect";
    qDebug() << Message::passwordWrong << "Message::passwordWrong";
    qDebug() << Message::profileInfoNotPresent << "Message::profileInfoNotPresent";
    qDebug() << Message::serverNotFound << "Message::serverNotFound";
    qDebug() << Message::successfulLogin << "Message::successfulLogin";
    qDebug() << Message::successfulLogout << "Message::successfulLogout";
    qDebug() << Message::successfulRegistraton << "Message::successfulRegistraton";
    qDebug() << Message::telephonPresent << "Message::telephonPresent";
    qDebug() << Message::unknownError << "Message::unknownError";
}

void MessageManager::setFlagMsg(Message msg, Flags flag, Flags spaceFlag)
{
    qDebug() << "MessageManager::setFlagMsg(Message msg, Flags flag, Flags spaceFlag)";
    setFlagMsg(msg, "FM_Base", spaceFlag);
    qDebug() << '\n';
    if (flag != Flags::unknown)
        emit sendFlag(flag, msg);
}

void MessageManager::setFlagMsg(Message msg, QString nameModule, Flags spaceFlag)
{
    QString text;
    switch(msg){
    case Message::accountNotPresent:
        text = tr("Аккаунт отсутствует");
        break;
    case Message::loginPresent:
        text = tr("Логин уже существует");
        break;
    case Message::connectToServer:
        text = tr("Соединение с сервером установлено");
        break;
    case Message::emailFree:
        text = tr("Почта свободна");
        break;
    case Message::emailIncorrect:
        text = tr("Почта задана некоректно");
        break;
    case Message::emailNotPresent:
        text = tr("Почта не найдена");
        break;
    case Message::emailPresent:
        text = tr("Почта используеться");
        break;
    case Message::insecurePassword:
        text = tr("Пароль незащищен");
        break;
    case Message::loginFree:
        text = tr("Логин свободен");
        break;
    case Message::passwordDoNotMatch:
        text = tr("Пароль не подходит");
        break;
    case Message::passwordIncorrect:
        text = tr("Пароль некоректен");
        break;
    case Message::passwordWrong:
        text = tr("Ошибка пароля");
        break;
    case Message::profileInfoNotPresent:
        text = tr("Файл профиля отсутствует");
        break;
    case Message::serverNotFound:
        text = tr("Сервер не найден");
        break;
    case Message::successfulLogin:
        text = tr("Вход успешен");
        break;
    case Message::successfulLogout:
        text = tr("Выход успешен");
        break;
    case Message::successfulRegistraton:
        text = tr("Регистрация успешна");
        break;
    case Message::unknownError:
    default:
        text = tr("Неизвестная ошибка");
    }
    qDebug() << text << msg;

    if (msg >= 0x2000)
        setStringMsg(nameModule, text, spaceFlag, Flags::critical);
    else if (msg >= 0x1000)
        setStringMsg(nameModule, text, spaceFlag, Flags::warning);
    else
        setStringMsg(nameModule, text, spaceFlag, Flags::information);
}

void MessageManager::setStringMsg(QString msg, QString nameModule, Flags spaceFlag, Flags levelFlag)
{
    qDebug() << "MessageManager::setStringMsg 1";
    QDateTime dTime = QDateTime::currentDateTime();
    messageList [dTime] [int()] [nameModule] = msg;
    emit sendMsg(nameModule, msg, spaceFlag, levelFlag, dTime);
    qDebug() << "MessageManager::setStringMsg 2";
}

void MessageManager::saveMsgList()
{
    file->open(QIODevice::WriteOnly);
    stream.setDevice(file);
    stream << messageList;
    file->close();
    qDebug() << "saveMsgList" << messageList << "\n" << QDateTime::currentDateTime();
}
