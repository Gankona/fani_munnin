#include "accountinfo.h"

AccountInfo::AccountInfo()
{
    name = "";
    login = "";
    password = "";
    helpWord = "";
    telephon = "";
    timeLogin.clear();
    isNoName = true;

    //server
    hostList.clear();
    mainHost = "127.0.0.1";
    mainPort = 32505;
    qDebug() << "AccountInfo create";
}

QDataStream &operator <<(QDataStream &s, AccountInfo &a)
{
    s << a.name << a.login << a.password << a.helpWord << a.telephon
      << a.timeLogin << a.hostList << a.mainHost << a.mainPort << a.isNoName;
    return s;
}

QDataStream &operator >>(QDataStream &s, AccountInfo &a)
{
    s >> a.name >> a.login >> a.password >> a.helpWord >> a.telephon
      >> a.timeLogin >> a.hostList >> a.mainHost >> a.mainPort >> a.isNoName;
    return s;
}
