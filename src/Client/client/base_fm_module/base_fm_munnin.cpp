#include "base_fm_munnin.h"

extern MessageManager message;
extern AccountInfo accountInfo;

Base_fm_munnin::Base_fm_munnin() : setting()
{
    currentWidget = 0;
}

void Base_fm_munnin::startFaniMunnin()
{
    //создаем окно
    view = new View;
    view->getPanelBar()->setNewButton(true, QIcon("://home.jpg"), tr("домой"));

    //создаем сервер
    //serverThread = new QThread(this);
    qDebug() << '\n' << "startServer" << '\n';
    server = new Server;
    qDebug() << '\n' << "endServer" << '\n';
    //server->moveToThread(serverThread);
    //serverThread->start();

    //ищем модули
    this->findModuls();

    qDebug() << '\n' << "endFileModule" << '\n';
    //авторизируемся
    //перезаписываем настройки
    slotAcceptSetting();
    qDebug() << '\n' << "endSlotAcceptSetting" << '\n';

    //ищем адоны
    this->findAddons();
    qDebug() << '\n' << "endFindAddons" << '\n';

    //показ окна
    view->show();
    qDebug() << '\n' << "endView" << '\n';

    QObject::connect(qApp, SIGNAL(lastWindowClosed()), this, SLOT(slotCloseApp()));
    QObject::connect(view, SIGNAL(setCurrentWidget(int)), this, SLOT(slotSetModule(int)));
    QObject::connect(view, SIGNAL(login(QString,QString)), server, SLOT(setLogin(QString,QString)));
    QObject::connect(view, SIGNAL(registration(AccountInfo*)), server, SLOT(setRegistration(AccountInfo*)));
    QObject::connect(view, SIGNAL(logout()), server, SLOT(logout()));
    message.setStringMsg("Base_FM_Munnin create");
}

void Base_fm_munnin::slotCloseApp()
{
    qDebug() << "closeApp";
    foreach (ModuleList *m, baseList)
        m->module->closeApp();
    message.setStringMsg("Application close correct");
}

void Base_fm_munnin::openEditAccount(){}

void Base_fm_munnin::findAddons(){}

void Base_fm_munnin::findModuls()
{
    qDebug() << 123;
    bool isLoaded = false;
    qDebug() << 124;
    QDir dir(QDir::currentPath());
    qDebug() << 125;
    if (dir.cd("modules")){
        qDebug() << 126;
        foreach (QString fileName, dir.entryList(QDir::Files)) {
            qDebug() << 12343 << fileName;
            QPluginLoader pluginLoader(dir.absoluteFilePath(fileName));
            QObject *plugin = pluginLoader.instance();
            qDebug() << fileName << pluginLoader.errorString();
            if (plugin) {
                BaseModuleInterface *module = qobject_cast<BaseModuleInterface*>(plugin);
                baseList.push_back(new ModuleList(module,
                                                  view->getPanelBar()->setNewButton(
                                                      true,
                                                      module->getMainIcon(),
                                                      module->getModuleName())));
                QObject::connect(module, SIGNAL(goHome()), this, SLOT(slotSetHome()));
                QObject::connect(module, SIGNAL(applyChange()), this, SLOT(slotAcceptSetting()));
            }
        }
    }
    qDebug() << 127;

    if (!isLoaded) {
        QMessageBox box;
        box.setText("No one module not load.");
        box.show();
    }
}

void Base_fm_munnin::slotAcceptSetting()
{
    qApp->setStyle(QStyleFactory::create(setting.value("Interface/style").toString()));
    qApp->setFont(QFont(setting.value("Interface/font").toString()));
    view->resize(setting.value("Graphic/currentSize").toSize());
    view->move(setting.value("Graphic/currentPos").toPoint());
    setting.value("Graphic/isFixedSizeWindow").toBool()
            ? setting.value("Graphic/isFixedStartWindowSize").toBool()
              ? view->setFixedSize(setting.value("Graphic/startFixedWindowSize").toSize())
              : view->setFixedSize(setting.value("Graphic/currentSize").toSize())
            : view->resize(setting.value("Graphic/currentSize").toSize());


    //if (setting.value("Graphic/isFixedSizeWindow").toBool())
      //  view->setFixedSize(view->size());
    //view->setVisibleMenu(setting.value("Graphic/isMenuVisible").toBool());
    view->replaceWidgets(view->size());

//    setting.setValue("isCurrentVisibleMenu", isCurrentVisibleMenu);
//    setting.setValue("isCurrentVisibleWindow", isCurrentVisibleWindow);
//    setting.setValue("currentSize", currentSize);
//    setting.setValue("currentPos", currentPos);

//    setting.setValue("isFixedSizeWindow", isFixedSizeWindow);
//    setting.setValue("isChangeSizeRateably", isChangeSizeRateably);
//    setting.setValue("isMenuVisible", isMenuVisible);
//    setting.setValue("isAlwaysShowWindow", isAlwaysShowWindow);
//    setting.setValue("isFloatLoginWindow", isFloatLoginWindow);
//    setting.setValue("isFixedStartWindowSize", isFixedStartWindowSize);
//    setting.setValue("startFixedWindowSize", startFixedWindowSize);
//    setting.setValue("minimumSize", minimumSize);
//    setting.setValue("rateablyWindowScretch", rateablyWindowScretch);
}

void Base_fm_munnin::slotSetModule(int i)
{
    if (currentWidget == i)
        return;
    currentWidget = i;
    if (i == 0){
        view->home->setHome();
        return;
    }
    foreach (ModuleList *m, baseList)
        i == m->position ? view->home->setGuestModul(m->module->viewPluginWidget())
                         : m->module->deleteWidget();
}

void Base_fm_munnin::slotSetHome()
{
    view->home->setHome();
    currentWidget = 0;
}

void Base_fm_munnin::slotCatchLogin(Flags flag, Message msg)
{
    if (flag == Flags::login && msg == Message::successfulLogin)
        foreach (ModuleList *m, baseList)
            m->module->setLogin(accountInfo.login);
}
