#include "upwidget.h"

extern AccountInfo accountInfo;
extern MessageManager message;

UpWidget::UpWidget(QWidget *parent, int height) : QFrame(parent), setting()
{
    timer = new QTimer(this);
    timer->setInterval(1000);
    dateLabel = new QLabel(" " + QDateTime::currentDateTime()
                           .toString(setting.value("Interface/dateTimeFormat").toString()), this);
    loginLabel = new QLabel(this);
    dateLabel->setAlignment(Qt::AlignLeft);
    loginLabel->setAlignment(Qt::AlignRight);
    accountButton = MService::flatSquarePictureButton(QIcon(":/home.jpg"), this, tr(""));
    logoutButton = MService::flatSquarePictureButton(QIcon(":/home.jpg"), this, tr(""));
    this->setFixedHeight(height);
    this->show();

    QObject::connect(accountButton, SIGNAL(clicked(bool)), this, SIGNAL(account()));
    QObject::connect(logoutButton,  SIGNAL(clicked(bool)), this, SIGNAL(logout ()));
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
    QObject::connect(&message, SIGNAL(sendFlag(Flags,Message)), this, SLOT(setLogin(Flags,Message)));
    timer->start();
}

void UpWidget::resizeEvent(QResizeEvent *e)
{
    dateLabel->move(0, 0);
    dateLabel->setFixedSize(e->size().width()-150, e->size().height());
    loginLabel->move(dateLabel->width(), 0);
    loginLabel->setFixedSize(100, e->size().height());
    accountButton->move(e->size().width() - 50, 0);
    logoutButton->move(e->size().width() - 25, 0);
}

void UpWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->pos().x() > loginLabel->x()
            && e->pos().x() < loginLabel->x()+loginLabel->width()
            && e->pos().y() > loginLabel->y()
            && e->pos().y() < loginLabel->y()+loginLabel->height())
        emit account();
}

void UpWidget::updateTime()
{
    dateLabel->setText(" " + QDateTime::currentDateTime()
                       .toString(setting.value("Interface/dateTimeFormat").toString()));
}

void UpWidget::setLogin(Flags flag, Message msg)
{
    qDebug() << "void UpWidget::setLogin(Flags flag, Message msg)" << flag << msg;
    qDebug() << "Flags::login" << Flags::login << "Message::successfulLogin" << Message::successfulLogin;
    if (flag == Flags::login && msg == Message::successfulLogin)
        loginLabel->setText(accountInfo.login + " ("
                            + accountInfo.name + ") ");
    if (flag == Flags::logout && msg == Message::successfulLogout)
        loginLabel->setText(tr("Никто"));
}
