#include "homewidget.h"

HomeWidget::HomeWidget(QWidget *parent) : QFrame(parent)
{
    guest = nullptr;
    home = nullptr;
    this->show();
}

void HomeWidget::resizeEvent(QResizeEvent *e)
{
    if (guest != nullptr)
        guest->setFixedSize(e->size());
    if (home != nullptr)
        home->setFixedSize(e->size());
}

void HomeWidget::setGuestModul(QWidget *wgt)
{
    guest = wgt;
    guest->setParent(this);
    guest->setFixedSize(this->size());
    guest->show();
    if (home != nullptr){
        delete home;
        home = nullptr;
    }
}

void HomeWidget::setHome()
{
    if (home != nullptr)
        return;
    guest = nullptr;
    home = new QFrame(this);
    home->setStyleSheet("background: olive");
    home->show();
    home->setFixedSize(this->size());
}
