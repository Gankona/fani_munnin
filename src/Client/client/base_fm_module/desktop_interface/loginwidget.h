#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include "accountinfo.h"
#include "../control/messagemanager.h"
#include "login/autorizationwidget.h"
#include "login/editwidget.h"
#include "login/infowidget.h"
#include "login/registrationwidget.h"
#include "../../fm_namespace.h"

#include <QtCore/QObject>
#include <QtWidgets/QWidget>

using namespace FM::Flags;
using namespace FM::Message;

namespace LoginStatus{
enum Enum{
    info = 0,
    avtorization = 1,
    edit = 2,
    registration = 3
};
}

class LoginWidget : public QObject
{
    Q_OBJECT
    //Q_ENUM(Flags)
    //Q_ENUM(Message)

private:
    AutorizationWidget *avtorWidget;
    RegistrationWidget *regWidget;
    EditWidget *editWidget;
    InfoWidget *infoWidget;
    QWidget *parentWidget;

    bool isVisibleLogin;
    int currentWidget;
    QSize currentChildSize;

public:
    LoginWidget(QWidget *parent = nullptr);
    void setCurrentWindow(LoginStatus::Enum i);
    bool isVisibleLoginWindow();
    void setLoginWidgetPararmetr(QPoint pos, QSize size);
    void setVisibleLoginWidget(bool visible);
    int width();
    int height();

signals:
    void editProfile(AccountInfo *);
    void registrate(AccountInfo *);
    void login(QString, QString);
    void logout();

public slots:
    void clickToAccount();
    void switchWindow();
    void sendFromServer(Flags flag, int msg);

private slots:
    void catchMessageFlag(Flags flag, Message msg);
};

#endif // LOGINWIDGET_H
