#include "loginwidget.h"
#include "view.h"

extern AccountInfo accountInfo;
extern MessageManager message;

LoginWidget::LoginWidget(QWidget *parent)
    : parentWidget(parent)
{
    avtorWidget = nullptr;
    editWidget = nullptr;
    infoWidget = nullptr;
    regWidget = nullptr;
    setVisibleLoginWidget(false);

    //qRegisterMetaType <Flags> ("Flags");
    //qRegisterMetaType <Message> ("Message");

    QObject::connect(&message, SIGNAL(sendFlag(Flags,Message)), this, SLOT(catchMessageFlag(Flags,Message)));
}

void LoginWidget::setCurrentWindow(LoginStatus::Enum i)
{
    setVisibleLoginWidget(false);

    switch(i){
    case LoginStatus::info:
        infoWidget = new InfoWidget(parentWidget);
        infoWidget->show();
        currentChildSize = infoWidget->minimumSize();
        break;

    case LoginStatus::avtorization:
        avtorWidget = new AutorizationWidget(parentWidget);
        avtorWidget->show();
        currentChildSize = avtorWidget->minimumSize();
        QObject::connect(avtorWidget, SIGNAL(sendToServerLogin(QString,QString)),
                         this, SIGNAL(login(QString,QString)));
        QObject::connect(avtorWidget, SIGNAL(registration()), this, SLOT(switchWindow()));
        break;

    case LoginStatus::edit:
        editWidget = new EditWidget(parentWidget);
        editWidget->show();
        currentChildSize = editWidget->minimumSize();
        break;

    case LoginStatus::registration:
        regWidget = new RegistrationWidget(parentWidget);
        regWidget->show();
        currentChildSize = regWidget->minimumSize();
        QObject::connect(regWidget, SIGNAL(registrate(AccountInfo*)), this, SIGNAL(registrate(AccountInfo*)));
        QObject::connect(regWidget, SIGNAL(back()), this, SLOT(switchWindow()));
        break;

    default:;
    }

    isVisibleLogin = true;
    static_cast<View*>(parentWidget)->replaceWidgets(parentWidget->size());
}

bool LoginWidget::isVisibleLoginWindow()
{
    return isVisibleLogin;
}

void LoginWidget::setLoginWidgetPararmetr(QPoint pos, QSize size)
{
    if (infoWidget != nullptr){
        infoWidget->move(pos);
        infoWidget->setFixedSize(size);
    }
    if (avtorWidget != nullptr){
        avtorWidget->move(pos);;
        avtorWidget->setFixedSize(size);
    }
    if (regWidget != nullptr){
        regWidget->move(pos);;
        regWidget->setFixedSize(size);
    }
    if (editWidget != nullptr){
        editWidget->move(pos);;
        editWidget->setFixedSize(size);
    }
    currentChildSize = size;
}

void LoginWidget::setVisibleLoginWidget(bool visible)
{
    qDebug() << accountInfo.login << accountInfo.isNoName;
    if (visible)
        accountInfo.isNoName ? setCurrentWindow(LoginStatus::avtorization)
                             : setCurrentWindow(LoginStatus::info);
    else {
        if (infoWidget != nullptr){
            delete infoWidget;
            infoWidget = nullptr;
        }
        if (avtorWidget != nullptr){
            delete avtorWidget;
            avtorWidget = nullptr;
        }
        if (regWidget != nullptr){
            delete regWidget;
            regWidget = nullptr;
        }
        if (editWidget != nullptr){
            delete editWidget;
            editWidget = nullptr;
        }
    }
    isVisibleLogin = visible;
}

int LoginWidget::width()
{
    return currentChildSize.width();
}

int LoginWidget::height()
{
    return currentChildSize.height();
}

void LoginWidget::clickToAccount()
{
    setVisibleLoginWidget(! isVisibleLogin);
}

void LoginWidget::sendFromServer(Flags flag, int msg){}

void LoginWidget::switchWindow()
{
    if (sender() == avtorWidget)
        setCurrentWindow(LoginStatus::registration);
    else if (sender() == regWidget)
        setCurrentWindow(LoginStatus::avtorization);
    else if (sender() == editWidget)
        setCurrentWindow(LoginStatus::info);
    qDebug() << "bu()";
}

void LoginWidget::catchMessageFlag(Flags flag, Message msg)
{
    qDebug() << flag << msg << "LoginWidget::catchMessageFlag";
    switch (flag) {
    case Flags::registration:
        if (msg == Message::successfulRegistraton)
            setVisibleLoginWidget(false);
        break;
    case Flags::login:
        if (msg == Message::successfulLogin)
            setVisibleLoginWidget(false);
        break;
    default:;
    }
}
