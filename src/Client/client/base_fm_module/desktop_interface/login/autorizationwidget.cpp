#include "autorizationwidget.h"

AutorizationWidget::AutorizationWidget(QWidget *parent) : QFrame(parent)
{
    this->setStyleSheet("background: black; color: white");
    qDebug() << "Autorization create";

    loginLabel = new QLabel(tr("логин или ел.почта"), this);
    titleLabel = new QLabel(tr("Авторизация"), this);
    titleLabel->setAlignment(Qt::AlignCenter);
    passwordLabel = new QLabel(tr("пароль"), this);
    loginEdit = new QLineEdit(this);
    passwordEdit = new QLineEdit(this);
    switchPasswordEcho();
    loginButton = new QPushButton(tr("войти"), this);
    registrationButton = MService::flatButton(this, "R", QSize(Y, Y));
    passwordSwitcherButton = MService::flatSquarePictureButton(QIcon(":/home.jpg"), this, tr(""), QSize(Y, Y));
    this->setMinimumSize(250, 6*Y+23);

    QObject::connect(registrationButton, SIGNAL(clicked(bool)), this, SIGNAL(registration()));
    QObject::connect(passwordEdit, SIGNAL(returnPressed()), this, SLOT(sendToServer()));
    QObject::connect(loginEdit, SIGNAL(returnPressed()), passwordEdit, SLOT(setFocus()));
    QObject::connect(loginButton, SIGNAL(clicked(bool)), this, SLOT(sendToServer()));
    QObject::connect(passwordSwitcherButton, SIGNAL(clicked(bool)), this, SLOT(switchPasswordEcho()));
    this->show();
    //this->parentWidget()->show();
}

void AutorizationWidget::resizeEvent(QResizeEvent *e)
{
    titleLabel->setFixedSize(e->size().width() - Y, Y);
    titleLabel->move(0, 0);
    registrationButton->move(titleLabel->width(), 0);
    loginLabel->setFixedSize(e->size().width() - 10, Y);
    loginLabel->move(10, Y+5*spacing);
    loginEdit->setFixedSize(e->size().width(), Y);
    loginEdit->move(0, 2*Y+6*spacing);
    passwordLabel->setFixedSize(e->size().width() - 10 - Y, Y);
    passwordLabel->move(10, 3*Y+7*spacing);
    passwordSwitcherButton->move(e->size().width() - Y, 3*Y+7*spacing);
    passwordEdit->setFixedSize(e->size().width(), Y);
    passwordEdit->move(0, 4*Y+8*spacing);
    loginButton->setFixedSize(e->size().width(), Y);
    loginButton->move(0, 5*Y+9*spacing);
}

void AutorizationWidget::sendToServer()
{
    emit sendToServerLogin(loginEdit->text(), passwordEdit->text());
}

void AutorizationWidget::switchPasswordEcho()
{
    isEchoPasswordMode = !isEchoPasswordMode;
    isEchoPasswordMode ? passwordEdit->setEchoMode(QLineEdit::Password)
                       : passwordEdit->setEchoMode(QLineEdit::Normal);
}
