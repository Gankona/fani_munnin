#ifndef REGISTRATIONWIDGET_H
#define REGISTRATIONWIDGET_H

#include "accountinfo.h"
#include "mservice.h"
#include "../../fm_namespace.h"

#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

using namespace FM::Flags;

class RegistrationWidget : public QFrame
{
    Q_OBJECT
private:
    QLabel *regLabel;
    QMap <QString, QLineEdit*> lineList;
    QMap <QString, QLabel*>  labelsList;
    QStringList nameList;
    QPushButton *cleanButton;
    QPushButton *regButton;
    QPushButton *backButton;

    QLabel *passwordSwitcherLabel;
    QPushButton *passwordSwitcher;
    const int Y = 20;
    const int spacing = 2;
    bool isEchoPasswordMode = false;

    inline void setNewLine(QString name, QString labelName);

protected:
    void resizeEvent(QResizeEvent *e);

public:
    explicit RegistrationWidget(QWidget *parent = 0);
    ~RegistrationWidget(){
        qDebug() << "RegistrationWidget delete";
    }

signals:
    void registrate(AccountInfo*);
    void back();

private slots:
    void cleanAll();
    void tryRegistrate();
    void switchPasswordEcho();

public slots:
    void messageSlot(QString moduleName,
                     QString text,
                     Flags spaceFlag,
                     Flags levelFlag,
                     QDateTime d);
};

#endif // REGISTRATIONWIDGET_H
