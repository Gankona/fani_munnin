#ifndef AUTORIZATIONWIDGET_H
#define AUTORIZATIONWIDGET_H

#include "mservice.h"

#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

class AutorizationWidget : public QFrame
{
    Q_OBJECT

private:
    QLabel *loginLabel;
    QLabel *titleLabel;
    QLabel *passwordLabel;
    QLineEdit *loginEdit;
    QLineEdit *passwordEdit;
    QPushButton *loginButton;
    QPushButton *registrationButton;
    QPushButton *passwordSwitcherButton;
    bool isEchoPasswordMode = false;
    const int Y = 20;
    const int spacing = 2;

protected:
    void resizeEvent(QResizeEvent *e);

public:
    explicit AutorizationWidget(QWidget *parent = 0);
    ~AutorizationWidget(){
        qDebug() << "AutorizationWidget delete";
    }

signals:
    void registration();
    void sendToServerLogin(QString, QString);

private slots:
    void sendToServer();
    void switchPasswordEcho();
};

#endif // AUTORIZATIONWIDGET_H
