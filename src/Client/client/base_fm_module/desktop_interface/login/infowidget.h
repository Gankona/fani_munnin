#ifndef INFOWIDGET_H
#define INFOWIDGET_H

#include <QWidget>
#include <QDebug>

class InfoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit InfoWidget(QWidget *parent = 0);
    ~InfoWidget(){
        qDebug() << "delete infoWidget";
    }

signals:

public slots:
};

#endif // INFOWIDGET_H
