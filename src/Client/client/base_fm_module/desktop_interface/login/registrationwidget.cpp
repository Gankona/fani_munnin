#include "registrationwidget.h"
#include "../../control/messagemanager.h"

extern MessageManager message;

RegistrationWidget::RegistrationWidget(QWidget *parent) : QFrame(parent)
{
    qDebug() << "RegistrationWidget create";
    nameList << "Login"
             << "Name"
             << "Email"
             << "Telephon"
             << "Password"
             << "Repeat password"
             << "Help word";

    regLabel = MService::alignCenterLabel(this, tr("Регистрация"), Y);
    regButton = MService::flatButton(this, tr("Зарегистрировать"));
    backButton = MService::flatButton(this, "<-", QSize(Y, Y));
    cleanButton = MService::flatSquarePictureButton(QIcon(":/home.jpg"), this, tr("очистить"), QSize(Y, Y));
    passwordSwitcher = MService::flatSquarePictureButton(QIcon(":/home.jpg"), this, "", QSize(Y, Y));

    setNewLine(nameList.at(0), tr("Логин"));
    setNewLine(nameList.at(1), tr("Имя"));
    setNewLine(nameList.at(2), tr("Почта"));
    setNewLine(nameList.at(3), tr("Телефон"));
    setNewLine(nameList.at(4), tr("Пароль"));
    setNewLine(nameList.at(5), tr("Повторить пароль"));
    setNewLine(nameList.at(6), tr("Подсказка"));

    this->setMinimumSize(200, 60*(nameList.length()+1));
    cleanAll();
    switchPasswordEcho();

    for (int i = 0; i < nameList.length(); i++){
        QString s = nameList.at(i);
        if (s == nameList.last())
            QObject::connect(lineList.value(s), SIGNAL(returnPressed()), this, SLOT(tryRegistrate()));
        else
            QObject::connect(lineList.value(s), SIGNAL(returnPressed()), lineList.value(nameList.at(i+1)), SLOT(setFocus()));
    }

    QObject::connect(regButton, SIGNAL(clicked(bool)), this, SLOT(tryRegistrate()));
    QObject::connect(backButton, SIGNAL(clicked(bool)), this, SIGNAL(back()));
    QObject::connect(cleanButton, SIGNAL(clicked(bool)), this, SLOT(cleanAll()));
    QObject::connect(passwordSwitcher, SIGNAL(clicked(bool)), this, SLOT(switchPasswordEcho()));
    QObject::connect(&message, SIGNAL(sendMsg    (QString, QString, Flags, Flags, QDateTime)),
                     this,     SLOT  (messageSlot(QString, QString, Flags, Flags, QDateTime)));
}

void RegistrationWidget::messageSlot(QString moduleName,
                                     QString text,
                                     Flags spaceFlag,
                                     Flags levelFlag,
                                     QDateTime d)
{
    if (spaceFlag == Flags::showInDestination){
        qDebug() << "RegistrationWidget::messageSlot" << text << d;
    }
}

void RegistrationWidget::resizeEvent(QResizeEvent *e)
{
    backButton->move(0, 0);
    cleanButton->move(e->size().width() - Y, 0);
    regLabel->move(Y, 0);
    regLabel->setFixedSize(e->size().width() - 2*Y, Y);
    for (int i = 0; i < nameList.length(); i++){
        if (i == 4)
            labelsList.value(nameList.at(i))->setFixedSize(e->size().width()-10-Y, Y);
        else
            labelsList.value(nameList.at(i))->setFixedSize(e->size().width()-10, Y);
        labelsList.value(nameList.at(i))->move(5, (2*i+1)*(Y+spacing));
        lineList.value(nameList.at(i))->move(0, (2*i+2)*(Y+spacing));
        lineList.value(nameList.at(i))->setFixedSize(e->size().width(), Y);
    }
    passwordSwitcher->move(e->size().width() - Y, 9*(Y+spacing));
    regButton->move(0, (nameList.length()*2+1)*(Y+spacing));
    regButton->setFixedSize(e->size().width(), Y);
}

void RegistrationWidget::setNewLine(QString name, QString labelName)
{
    labelsList.insert(name, new QLabel(labelName, this));
    lineList.insert(name, new QLineEdit(this));
}

void RegistrationWidget::tryRegistrate()
{
    AccountInfo *account = new AccountInfo;
    account->helpWord = lineList.value("Help word")->text();
    account->isNoName = false;
    account->login = lineList.value("Login")->text();
    account->name = lineList.value("Name")->text();
    account->password = lineList.value("Password")->text();
    account->telephon = lineList.value("Telephon")->text();
    account->email = lineList.value("Email")->text();
    emit registrate(account);
}

void RegistrationWidget::cleanAll()
{
    foreach (QLineEdit *l, lineList)
        l->clear();
    lineList.value("Login")->setFocus();
}

void RegistrationWidget::switchPasswordEcho()
{
    isEchoPasswordMode = !isEchoPasswordMode;
    if (isEchoPasswordMode){
        lineList.value("Password")->setEchoMode(QLineEdit::Password);
        lineList.value("Repeat password")->setEchoMode(QLineEdit::Password);
    }
    else {
        lineList.value("Password")->setEchoMode(QLineEdit::Password);
        lineList.value("Repeat password")->setEchoMode(QLineEdit::Password);
    }
}
