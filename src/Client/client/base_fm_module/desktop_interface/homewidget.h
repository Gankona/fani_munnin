#ifndef HOMEWIDGET_H
#define HOMEWIDGET_H

#include <QtGui/QResizeEvent>
#include <QtWidgets/QFrame>

class HomeWidget : public QFrame
{
    Q_OBJECT

private:
    QFrame *home;

public:
    HomeWidget(QWidget *parent = 0);
    QWidget *guest;

    void resizeEvent(QResizeEvent *e);
    void setGuestModul(QWidget *wgt);

signals:

public slots:
    void setHome();
};

#endif // HOMEWIDGET_H
