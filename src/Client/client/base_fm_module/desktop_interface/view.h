#ifndef VIEW_H
#define VIEW_H

#include "accountinfo.h"
#include "mservice.h"
#include "msidepanelbar.h"
#include "basemoduleinterface.h"
#include "../../fm_namespace.h"
#include "../control/messagemanager.h"

#include "upwidget.h"
#include "homewidget.h"
#include "menuwidget.h"
#include "loginwidget.h"

#include <QDebug>
#include <QtCore/QSettings>
#include <QtGui/QMoveEvent>
#include <QtGui/QResizeEvent>
#include <QtGui/QShowEvent>
#include <QtWidgets/QApplication>
#include <QtWidgets/QStyleFactory>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>

using namespace FM::Flags;
using namespace FM::Message;

class View : public QWidget
{
    Q_OBJECT
private:
    MenuWidget *menu;
    UpWidget *up;
    LoginWidget *loginWidget;
    MSidePanelBar *sideBar;
    QPushButton *threeLine;

    QSettings setting;

    int spacing;

    void resizeEvent(QResizeEvent *e);
    void moveEvent(QMoveEvent *e);
    void showEvent(QShowEvent *);

public:
    View();

    HomeWidget *home;
    MSidePanelBar* getPanelBar();

    void replaceWidgets(QSize size);
    void setVisibleMenu(bool isShow);

signals:
    void setCurrentWidget(int);
    void login(QString, QString);
    void registration(AccountInfo*);
    void logout();

private slots:
    void switchMenu();

public slots:
    void slotCatchMsg(QString moduleName, QString text, Flags spaceFlag, Flags levelFlag, QDateTime d);
};

#endif // VIEW_H
