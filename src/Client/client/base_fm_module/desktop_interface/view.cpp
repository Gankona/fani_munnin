#include "view.h"

extern MessageManager message;

View::View() : setting()
{
    spacing = 2;
    threeLine = MService::flatButton(this, "M", QSize(25,25));
    sideBar = new MSidePanelBar(this);
    up = new UpWidget(this);
    home = new HomeWidget(this);
    menu = new MenuWidget(this);
    loginWidget = new LoginWidget(this);
    //switchMenu();

//    QString styleName = setting.value("Interface/style", "default-winter").toString();
//    bool isFindStyle = false;
//    foreach (QString str, QStyleFactory::keys())
//        if (str == styleName)
//            isFindStyle = true;
//    if (! isFindStyle){
//        styleName = "default-winter";
//        setting.setValue("Interface/style", styleName);
//    }
//    qApp->setStyle(QStyleFactory::create(styleName));
//    qApp->setStyle(QStyleFactory::create("Breeze"));

    menu->setStyleSheet("background: grey");
    up->setStyleSheet("background: yellow");
    sideBar->setStyleSheet("background: white");

    QObject::connect(sideBar, SIGNAL(setCurrentWidget(int)), this, SIGNAL(setCurrentWidget(int)));
    QObject::connect(threeLine, SIGNAL(clicked(bool)), this, SLOT(switchMenu()));
    QObject::connect(up, SIGNAL(account()), loginWidget, SLOT(clickToAccount()));
    QObject::connect(up, SIGNAL(logout()), this, SIGNAL(logout()));
    QObject::connect(loginWidget, SIGNAL(logout()), this, SIGNAL(logout()));
    QObject::connect(loginWidget, SIGNAL(login(QString,QString)), this, SIGNAL(login(QString,QString)));
    QObject::connect(loginWidget, SIGNAL(registrate(AccountInfo*)), this, SIGNAL(registration(AccountInfo*)));
    QObject::connect(&message, SIGNAL(sendMsg(QString,QString,Flags,Flags,QDateTime)),
                     this, SLOT(slotCatchMsg(QString,QString,Flags,Flags,QDateTime)));
}

void View::showEvent(QShowEvent *)
{
    replaceWidgets(this->size());
}

void View::moveEvent(QMoveEvent *e)
{
    setting.setValue("Graphic/currentPos", e->pos());
}

void View::resizeEvent(QResizeEvent *e)
{
    replaceWidgets(e->size());
    setting.setValue("Graphic/currentSize", e->size());
}

void View::replaceWidgets(QSize size)
{
    int menuX(0);
    menu->setVisible(setting.value("Graphic/isCurrentVisibleMenu", false).toBool() && menu->isMenuShow);

    if (setting.value("Graphic/isCurrentVisibleMenu", false).toBool() && menu->isMenuShow){
        menuX = 100;
        menu->setFixedSize(menuX, size.height());
        menu->move(threeLine->width() + spacing, 0);
    }

    threeLine->move(0, 0);
    sideBar->setFixedHeight(size.height() - threeLine->height());
    sideBar->move(0, threeLine->height() + spacing);

    up->setFixedWidth(size.width() - threeLine->width() - menuX);
    up->move(threeLine->width() + menuX + 2*spacing, 0);
    home->move(threeLine->width() + menuX + 2*spacing, up->height() + spacing);
    if (!loginWidget->isVisibleLoginWindow()){
        home->setFixedSize(size.width() - menuX - sideBar->width(), size.height() - up->height());
    }
    else if (setting.value("Graphic/isFloatLoginWindow", false).toBool()){
        home->setFixedSize(size.width() - menuX - sideBar->width(), size.height() - up->height());
        loginWidget->setLoginWidgetPararmetr(QPoint(size.width() - (5 + loginWidget->width()), up->height() + 5),
                                             QSize(loginWidget->width(), loginWidget->height()));
    }
    else {
        home->setFixedSize(size.width() - menu->width() - sideBar->width() - loginWidget->width(),
                           size.height() - up->height());
        loginWidget->setLoginWidgetPararmetr(QPoint(size.width() - (5 + loginWidget->width()), up->height()),
                                             QSize(loginWidget->width(), home->height()));
    }
}

MSidePanelBar* View::getPanelBar()
{
    return sideBar;
}

void View::switchMenu()
{
    if (! setting.value("Graphic/isMenuVisible").toBool())
        return;
    setting.setValue("Graphic/isCurrentVisibleMenu", ! menu->isVisible());
    this->replaceWidgets(this->size());
}

void View::setVisibleMenu(bool isShow)
{
    menu->isMenuShow = isShow;
    replaceWidgets(this->size());
}

void View::slotCatchMsg(QString moduleName, QString text, Flags spaceFlag, Flags levelFlag, QDateTime d)
{
    qDebug() << text << spaceFlag << levelFlag << d;
}
