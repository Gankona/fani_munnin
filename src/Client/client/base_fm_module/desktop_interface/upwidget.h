#ifndef UPWIDGET_H
#define UPWIDGET_H

#include "mservice.h"
#include "accountinfo.h"
#include "../control/messagemanager.h"

#include <QtCore/QTimer>
#include <QtCore/QDateTime>
#include <QtCore/QSettings>
#include <QtGui/QMouseEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>

class UpWidget : public QFrame
{
    Q_OBJECT
private:
    QLabel *dateLabel;
    QLabel *loginLabel;
    QPushButton *accountButton;
    QPushButton *logoutButton;
    QTimer *timer;
    QSettings setting;

protected:
    void resizeEvent(QResizeEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);

public:
    explicit UpWidget(QWidget *parent = 0, int height = 25);

signals:
    void logout();
    void account();

public slots:
    void updateTime();
    void setLogin(Flags flag, Message msg);
};

#endif // UPWIDGET_H
