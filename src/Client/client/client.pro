include($$PWD/../path.pri)

QT += widgets
QT += network

DESTDIR = $$BIN_PATH

SOURCES += \
    main.cpp \
    base_fm_module/control/messagemanager.cpp

SOURCES += \
    base_fm_module/desktop_interface/view.cpp \
    base_fm_module/base_fm_munnin.cpp \
    base_fm_module/control/server.cpp \
    base_fm_module/desktop_interface/login/registrationwidget.cpp \
    base_fm_module/desktop_interface/login/infowidget.cpp \
    base_fm_module/desktop_interface/login/editwidget.cpp \
    base_fm_module/desktop_interface/loginwidget.cpp \
    base_fm_module/desktop_interface/login/autorizationwidget.cpp

HEADERS += \
    $$SRC_PATH/modules/basemoduleinterface.h \
    base_fm_module/desktop_interface/view.h \
    base_fm_module/base_fm_munnin.h \
    base_fm_module/control/server.h \
    base_fm_module/desktop_interface/login/registrationwidget.h \
    base_fm_module/desktop_interface/login/infowidget.h \
    base_fm_module/desktop_interface/login/editwidget.h \
    base_fm_module/desktop_interface/loginwidget.h \
    base_fm_module/desktop_interface/login/autorizationwidget.h \
    base_fm_module/control/messagemanager.h

include($$SRC_PATH/libs/mservice/mservice.pri)
include($$SRC_PATH/libs/msidepanelbar/msidepanelbar.pri)
include($$SRC_PATH/libs/mopacitywidget/mopacitywidget.pri)
include($$SRC_PATH/libs/mpulldownwidget/mpulldownwidget.pri)
include($$SRC_PATH/libs/mswitcherpos/mswitcherpos.pri)
include($$SRC_PATH/libs/accountinfo/accountinfo.pri)

SOURCES += base_fm_module/desktop_interface/upwidget.cpp \
           base_fm_module/desktop_interface/menuwidget.cpp \
           base_fm_module/desktop_interface/homewidget.cpp

HEADERS += base_fm_module/desktop_interface/upwidget.h \
           base_fm_module/desktop_interface/menuwidget.h \
           base_fm_module/desktop_interface/homewidget.h

#INCLUDEPATH += $$SRC_PATH/client
INCLUDEPATH += $$SRC_PATH/modules

RESOURCES += \
    base_fm_module/resource.qrc
