#include "accountinfo.h"
#include "base_fm_module/base_fm_munnin.h"
#include "base_fm_module/control/messagemanager.h"

#include <QtWidgets/QApplication>

AccountInfo accountInfo;
MessageManager message;

int main(int argc, char **argv){

    QApplication app(argc, argv);
    app.setOrganizationName("Gankona");
    app.setApplicationName("Fani Munnin");
    message.setStringMsg("Project started");

    Base_fm_munnin *base_fm = new Base_fm_munnin;
    base_fm->startFaniMunnin();

    return app.exec();
}
