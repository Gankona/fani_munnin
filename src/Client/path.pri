CONFIG += c++14

isEmpty(path.pri){
    SRC_PATH = $$PWD

    win32 {
        CONFIG(debug,   debug|release): BIN_PATH = $$PWD/../../desktop_debug
        CONFIG(release, debug|release): BIN_PATH = E:/FaniMunnin
        CONFIG(release, debug|release): LIBM_PATH = E:/FaniMunnin/modules/libs
        CONFIG(release, debug|release): LIBA_PATH = E:/FaniMunnin/addons/libs
    }
    linux:!android {
        CONFIG(release, debug|release): BIN_PATH = /opt/FaniMunnin
        CONFIG(release, debug|release): LIBM_PATH = /opt/FaniMunnin/modules/libs
        CONFIG(release, debug|release): LIBA_PATH = /opt/FaniMunnin/addons/libs
    }
    android {
        CONFIG(debug,   debug|release): BIN_PATH = $$PWD/../../mobile_debug
        CONFIG(release, debug|release): BIN_PATH = $$PWD/../../mobile_release
    }
}
