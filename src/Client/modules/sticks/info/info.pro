#-------------------------------------------------
#
# Project created by QtCreator 2016-01-25T13:59:58
#
#-------------------------------------------------

QT       += widgets

TARGET = sticks_info
TEMPLATE = lib

include($$PWD/../../../path.pri)

DESTDIR = $$LIBM_PATH/info

SOURCES += info.cpp

HEADERS += info.h

DISTFILES += \
    info.pri
