#ifndef BASEMODULEINTERFACE
#define BASEMODULEINTERFACE

#include <QtCore/QDir>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QtPlugin>
#include <QtCore/QPluginLoader>
#include <QtGui/QPixmap>
#include <QtWidgets/QWidget>
#include <QtWidgets/QMessageBox>

class BaseModuleInterface : public QObject
{
    Q_OBJECT
public:
    virtual ~BaseModuleInterface(){}
    virtual QString getModuleName() = 0;
    virtual QWidget* viewPluginWidget() = 0;
    virtual QWidget* getInfo() = 0;
    virtual QIcon getMainIcon() = 0;
    virtual void setAppDataDir(QDir dir) = 0;
    virtual void setLogin(QString login) = 0;
    virtual void deleteWidget() = 0;
    virtual void closeApp() = 0;

signals:
    virtual void goHome() = 0;
    virtual void applyChange() = 0;
    virtual void setStatusString(int status, QString str) = 0;
};

#define BaseModuleInterface_iid "org.gankona.FaniMunnin.BaseModuleInterface"
Q_DECLARE_INTERFACE(BaseModuleInterface, BaseModuleInterface_iid)

#endif // BASEMODULEINTERFACE
