#ifndef INTERFACE_H
#define INTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>

class Interfaces : public QObject
{
    Q_OBJECT
public:
    Interfaces();
    QSettings setting;

    QString language;
    QString style;
    int volume;
    QString dateTimeFormat;
    QString firstWeekDay;
    float timeZone;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, Interfaces &s);
    friend QDataStream &operator >> (QDataStream &d, Interfaces &s);

signals:
    void saveSetting();
};

#endif // INTERFACE_H
