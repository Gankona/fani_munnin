#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QtCore/QObject>
#include <QtCore/QSettings>

class Account : public QObject
{
    Q_OBJECT
public:
    Account();
    QSettings setting;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, Account &s);
    friend QDataStream &operator >> (QDataStream &d, Account &s);

signals:
    void saveSetting();
};

#endif // ACCOUNT_H
