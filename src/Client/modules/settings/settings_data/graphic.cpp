#include "graphic.h"

Graphic::Graphic() : setting("Gankona", "Fani Munnin")
{

}

void Graphic::setDefault()
{
    isCurrentVisibleMenu = false;
    isCurrentVisibleWindow = false;
    currentSize = QSize(600, 400);

    isFixedSizeWindow = false;
    isChangeSizeRateably = false;
    isMenuVisible = true;
    isAlwaysShowWindow = false;
    isFloatLoginWindow = true;
    isFixedStartWindowSize = false;
    startFixedWindowSize = QSize(600, 400);
    minimumSize = QSize(400, 300);

    this->applySetting();
}

void Graphic::applySetting()
{
    setting.setValue("Graphic/isCurrentVisibleMenu", isCurrentVisibleMenu);
    setting.setValue("Graphic/isCurrentVisibleWindow", isCurrentVisibleWindow);
    setting.setValue("Graphic/currentSize", currentSize);

    setting.setValue("Graphic/isFixedSizeWindow", isFixedSizeWindow);
    setting.setValue("Graphic/isChangeSizeRateably", isChangeSizeRateably);
    setting.setValue("Graphic/isMenuVisible", isMenuVisible);
    setting.setValue("Graphic/isAlwaysShowWindow", isAlwaysShowWindow);
    setting.setValue("Graphic/isFloatLoginWindow", isFloatLoginWindow);
    setting.setValue("Graphic/isFixedStartWindowSize", isFixedStartWindowSize);
    setting.setValue("Graphic/startFixedWindowSize", startFixedWindowSize);
    setting.setValue("Graphic/minimumSize", minimumSize);

    emit saveSetting();
}

QDataStream &operator << (QDataStream &d, Graphic &s)
{
    d << s.isCurrentVisibleMenu << s.isCurrentVisibleWindow << s.currentSize
      << s.isFixedSizeWindow << s.isChangeSizeRateably << s.isMenuVisible
      << s.isAlwaysShowWindow << s.isFloatLoginWindow << s.isFixedStartWindowSize
      << s.startFixedWindowSize << s.minimumSize;
    return d;
}

QDataStream &operator >> (QDataStream &d, Graphic &s)
{
    d >> s.isCurrentVisibleMenu >> s.isCurrentVisibleWindow >> s.currentSize
      >> s.isFixedSizeWindow >> s.isChangeSizeRateably >> s.isMenuVisible
      >> s.isAlwaysShowWindow >> s.isFloatLoginWindow >> s.isFixedStartWindowSize
      >> s.startFixedWindowSize >> s.minimumSize;
    return d;
}
