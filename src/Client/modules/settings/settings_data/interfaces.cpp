#include "interfaces.h"

Interfaces::Interfaces() : setting("Gankona", "Fani Munnin")
{

}

void Interfaces::setDefault()
{
    language = "rus";
    style = "default-winter";
    volume = 0;
    dateTimeFormat = "hh:MM dd.mm.yyyy";
    firstWeekDay = "mo";
    timeZone = 2;

    this->applySetting();
}

void Interfaces::applySetting()
{
    setting.setValue("Interface/language", language);
    setting.setValue("Interface/style", style);
    setting.setValue("Interface/volume", volume);
    setting.setValue("Interface/dateTimeFormat", dateTimeFormat);
    setting.setValue("Interface/firstWeekDay", firstWeekDay);
    setting.setValue("Interface/timeZone", timeZone);

    emit saveSetting();
}

QDataStream &operator << (QDataStream &d, Interfaces &s)
{
    d << s.language << s.style << s.volume << s.dateTimeFormat
      << s.firstWeekDay << s.timeZone;
    return d;
}

QDataStream &operator >> (QDataStream &d, Interfaces &s)
{
    d >> s.language >> s.style >> s.volume >> s.dateTimeFormat
      >> s.firstWeekDay >> s.timeZone;
    return d;
}
