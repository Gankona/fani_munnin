include($$PWD/../../../path.pri)

linux:!android || win32 {
    isEmpty(desktop.pri){
        LIBS += -L$$LIBM_PATH/data -lsettings_data
        INCLUDEPATH += $$SRC_PATH/modules/settings/settings_data
        DEPENDPATH  += $$SRC_PATH/modules/settings/settings_data
    }
}
