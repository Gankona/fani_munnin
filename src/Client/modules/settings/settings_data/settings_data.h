#ifndef SETTINGS_DATA_H
#define SETTINGS_DATA_H

#include "account.h"
#include "graphic.h"
#include "hotkey.h"
#include "interfaces.h"
#include "server.h"

#include <QtCore/qglobal.h>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QDataStream>

class Settings_data : public QObject
{
    Q_OBJECT
private:
    QDir dir;
    QString currentLogin;
    QSettings setting;

public:
    Settings_data();

    Account *account;
    Graphic *graphic;
    HotKey *hotKey;
    Interfaces *interFace;//interface in windows is a special word
    Server *server;

    void createSetting(QString login);

    friend QDataStream &operator << (QDataStream &d, Settings_data &s);
    friend QDataStream &operator >> (QDataStream &d, Settings_data &s);

signals:
    void updateSetting();

public slots:
    void setDefault();
    void saveSetting();
};

#endif // SETTINGS_DATA_H
