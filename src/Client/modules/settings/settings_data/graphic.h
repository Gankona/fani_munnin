#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QSize>
#include <QtCore/QDataStream>

class Graphic : public QObject
{
    Q_OBJECT
public:
    Graphic();
    QSettings setting;

    bool isCurrentVisibleMenu;
    bool isCurrentVisibleWindow;
    QSize currentSize;

    bool isFixedSizeWindow;
    bool isChangeSizeRateably;
    bool isMenuVisible;
    bool isAlwaysShowWindow;
    bool isFloatLoginWindow;
    bool isFixedStartWindowSize;
    QSize startFixedWindowSize;
    QSize minimumSize;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, Graphic &s);
    friend QDataStream &operator >> (QDataStream &d, Graphic &s);

signals:
    void saveSetting();
};

#endif // GRAPHIC_H
