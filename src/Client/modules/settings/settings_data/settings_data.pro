#-------------------------------------------------
#
# Project created by QtCreator 2016-01-25T15:15:07
#
#-------------------------------------------------

QT       -= gui

TARGET = settings_data
TEMPLATE = lib

include($$PWD/../../../path.pri)

DESTDIR = $$LIBM_PATH/data

SOURCES += settings_data.cpp \
    account.cpp \
    graphic.cpp \
    hotkey.cpp \
    interfaces.cpp \
    server.cpp

HEADERS += settings_data.h \
    account.h \
    graphic.h \
    hotkey.h \
    interfaces.h \
    server.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    settings_data.pri
