include($$PWD/../../../path.pri)

include($$SRC_PATH/libs/mservice/mservice.pri)
include($$SRC_PATH/libs/msidepanelbar/msidepanelbar.pri)
include($$SRC_PATH/libs/mcontentwidget/mcontentwidget.pri)
include($$SRC_PATH/libs/mmarginwidget/marginwidget.pri)

linux:!android || win32 {
    isEmpty(desktop.pri){
        LIBS += -L$$LIBM_PATH/view -lsettings_desktop
        INCLUDEPATH += $$SRC_PATH/modules/settings/desktop
        DEPENDPATH  += $$SRC_PATH/modules/settings/desktop
    }
}
