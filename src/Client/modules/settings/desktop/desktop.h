#ifndef DESKTOP_H
#define DESKTOP_H

#include "mcontentwidget.h"
#include "settings_data.h"

#include "accountview.h"
#include "graphicview.h"
#include "hotkeyview.h"
#include "interfaceview.h"
#include "serverview.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QSettings>

class Desktop : public MContentWidget
{
    Q_OBJECT
private:
    AccountView *account;
    GraphicView *graphic;
    HotKeyView  *hotKey;
    InterfaceView *interfaces;
    ServerView *server;

    QSettings settings;
    Settings_data *data;

public:
    Desktop(QWidget *parent = nullptr);
    Desktop(Settings_data *data, QWidget *parent = nullptr);
};

#endif // DESKTOP_H
