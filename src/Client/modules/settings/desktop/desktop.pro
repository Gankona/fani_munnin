#-------------------------------------------------
#
# Project created by QtCreator 2016-01-24T12:51:36
#
#-------------------------------------------------

QT       += widgets

TARGET = settings_desktop
TEMPLATE = lib
DEFINES += DESKTOP_LIBRARY

include($$PWD/../../../path.pri)
include($$SRC_PATH/libs/mservice/mservice.pri)
include($$SRC_PATH/libs/msidepanelbar/msidepanelbar.pri)
include($$SRC_PATH/libs/mcontentwidget/mcontentwidget.pri)
include($$SRC_PATH/libs/mmarginwidget/marginwidget.pri)
include($$SRC_PATH/modules/settings/settings_data/settings_data.pri)

DESTDIR = $$LIBM_PATH/view

SOURCES += desktop.cpp \
    accountview.cpp \
    graphicview.cpp \
    serverview.cpp \
    interfaceview.cpp \
    hotkeyview.cpp

HEADERS += desktop.h \
    accountview.h \
    graphicview.h \
    serverview.h \
    interfaceview.h \
    hotkeyview.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    desktop.pri
