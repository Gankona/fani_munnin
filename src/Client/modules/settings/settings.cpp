#include "settings.h"

Settings::Settings()
{
    qDebug() << "Settings 1";
    icon = QIcon("://settings.png");
    moduleName = tr("Настройки");
    qDebug() << "Settings 2";
    login = "NoName";
    view = nullptr;
    data = new Settings_data;
    qDebug() << "Settings 3";
    data->createSetting(login);
    qDebug() << "Settings 4";
    QObject::connect(data, SIGNAL(updateSetting()), this, SIGNAL(applyChange()));
    QObject::connect(data, SIGNAL(updateSetting()), this, SLOT(qq()));
}

QString Settings::getModuleName()
{
    return moduleName;
}

void Settings::qq()
{
    qDebug() << "qq? we updateSetting";
}

QWidget* Settings::viewPluginWidget()
{
    deleteWidget();
    view = new View(data);
    QObject::connect(view, SIGNAL(goHome()), this, SIGNAL(goHome()));
    return static_cast<QWidget*>(view);
}

QWidget* Settings::getInfo()
{
    QWidget *info = new QWidget;
    return info;
}

QIcon Settings::getMainIcon()
{
    return icon;
}

void Settings::setAppDataDir(QDir dir)
{
    appDir = dir;
}

void Settings::setLogin(QString login)
{
    this->login = login;
    data->createSetting(login);
}

void Settings::deleteWidget()
{
    qDebug() << "delete view";
    if (view != nullptr){
        delete view;
        view = nullptr;
    }
}

void Settings::closeApp()
{
    qDebug() << "here";
    data->updateSettingFromReg();
}
