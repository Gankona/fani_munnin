#-------------------------------------------------
#
# Project created by QtCreator 2016-01-24T12:54:15
#
#-------------------------------------------------

QT       += widgets

TARGET = settings_info
TEMPLATE = lib

include($$PWD/../../../path.pri)

DESTDIR = $$LIBM_PATH/info

DEFINES += INFO_LIBRARY

SOURCES += info.cpp

HEADERS += info.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    info.pri
