#-------------------------------------------------
#
# Project created by QtCreator 2016-01-25T15:19:02
#
#-------------------------------------------------

QT       += core gui

TARGET = settings
TEMPLATE = lib
CONFIG += plugin

include($$PWD/../../../path.pri)

DESTDIR = $$BIN_PATH/modules

linux:!android || win32 {
    include($$PWD/../desktop/desktop.pri)
}

android {
    include($$PWD/../mobile/mobile.pri)
}

INCLUDEPATH += $$SRC_PATH/modules

SOURCES += settings.cpp
HEADERS += settings.h \
        $$PWD/../../basemoduleinterface.h
DISTFILES += settings.json

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../info/info.pri)
include($$PWD/../settings_data/settings_data.pri)

RESOURCES +=
