#include "settings.h"

Settings::Settings()
{
    icon = QIcon(qApp->applicationDirPath() + "/styles/default-navy/settings.ico");
    moduleName = tr("Настройки");
    view = nullptr;
}

QString Settings::getModuleName()
{
    return moduleName;
}

QWidget* Settings::viewPluginWidget()
{
    view = new Desktop;
    return static_cast<QWidget*>(view);
}

QWidget* Settings::getInfo()
{
    info = new Info;
    return static_cast<QWidget*>(info);
}

QIcon Settings::getMainIcon()
{
    return icon;
}

void Settings::setAppDataDir(QDir dir)
{
    appDir = dir;
}

void Settings::setLogin(QString login)
{
    this->login = login;
}

void Settings::deleteWidget()
{
    if (view != nullptr)
        delete view;
}
