#ifndef SETTINGS_H
#define SETTINGS_H

#include "basemoduleinterface.h"
#include "desktop.h"
#include "info.h"

#include <QtCore/QCoreApplication>
#include <QtGui/QIcon>
#include <QDebug>

class Settings : public QObject, BaseModuleInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.gankona.FaniMunnin.BaseModuleInterface")
    Q_INTERFACES(BaseModuleInterface)

    Desktop *view;
    QIcon icon;
    Info *info;
    QString login;
    QString moduleName;
    QDir appDir;

public:
    Settings();
    QString getModuleName() Q_DECL_OVERRIDE;
    QWidget* viewPluginWidget() Q_DECL_OVERRIDE;
    QWidget* getInfo() Q_DECL_OVERRIDE;
    QIcon getMainIcon() Q_DECL_OVERRIDE;
    void setAppDataDir(QDir dir) Q_DECL_OVERRIDE;
    void setLogin(QString login) Q_DECL_OVERRIDE;
    void deleteWidget() Q_DECL_OVERRIDE;
};

#endif // SETTINGS_H
