#-------------------------------------------------
#
# Project created by QtCreator 2016-01-25T15:19:02
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = settings
TEMPLATE = lib
CONFIG += plugin

include($$PWD/../../path.pri)
DESTDIR = $$BIN_PATH/modules

INCLUDEPATH += $$SRC_PATH/modules

DISTFILES += settings.json

include($$SRC_PATH/libs/mservice/mservice.pri)
include($$SRC_PATH/libs/msidepanelbar/msidepanelbar.pri)
include($$SRC_PATH/libs/mcontentwidget/mcontentwidget.pri)
include($$SRC_PATH/libs/mmarginwidget/marginwidget.pri)
include($$SRC_PATH/libs/mswitcherpos/mswitcherpos.pri)
include($$SRC_PATH/libs/mhlistwidget/mhlistwidget.pri)
include($$SRC_PATH/libs/mkey/mkey.pri)

HEADERS += \
    desktop_interface/graphicview.h \
    desktop_interface/serverview.h \
    desktop_interface/interfaceview.h \
    desktop_interface/hotkeyview.h \
    settings.h \
    $$PWD/../basemoduleinterface.h \
    desktop_interface/view.h \
    data/settings_data.h \
    data/graphic.h \
    data/hotkey.h \
    data/interfaces.h \
    data/server.h \
    desktop_interface/styleexamplewidget.h

SOURCES += data/settings_data.cpp \
    data/graphic.cpp \
    data/hotkey.cpp \
    data/interfaces.cpp \
    data/server.cpp \
    desktop_interface/graphicview.cpp \
    desktop_interface/serverview.cpp \
    desktop_interface/interfaceview.cpp \
    desktop_interface/hotkeyview.cpp \
    settings.cpp \
    desktop_interface/view.cpp \
    desktop_interface/styleexamplewidget.cpp

RESOURCES += \
    resource.qrc
