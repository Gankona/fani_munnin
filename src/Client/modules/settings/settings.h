#ifndef SETTINGS_H
#define SETTINGS_H

//#ifndef Q_OS_WIN32
#include "desktop_interface/view.h"
//#endif
#include "basemoduleinterface.h"
#include "data/settings_data.h"

#include <QtCore/QCoreApplication>
#include <QtGui/QIcon>
#include <QtGui/QCloseEvent>
#include <QDebug>

class Settings : public BaseModuleInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.gankona.FaniMunnin.BaseModuleInterface")
    Q_INTERFACES(BaseModuleInterface)

private:
    View *view;
    QIcon icon;
    QString login;
    QString moduleName;
    QDir appDir;
    Settings_data *data;

public:
    Settings();
    QString getModuleName() Q_DECL_OVERRIDE;
    QWidget* viewPluginWidget() Q_DECL_OVERRIDE;
    QWidget* getInfo() Q_DECL_OVERRIDE;
    QIcon getMainIcon() Q_DECL_OVERRIDE;
    void setAppDataDir(QDir dir) Q_DECL_OVERRIDE;
    void setLogin(QString login) Q_DECL_OVERRIDE;
    void deleteWidget() Q_DECL_OVERRIDE;
    void closeApp() Q_DECL_OVERRIDE;

signals:
    void goHome() Q_DECL_OVERRIDE;
    void applyChange() Q_DECL_OVERRIDE;
    void setStatusString(int level, QString msg) Q_DECL_OVERRIDE;

public slots:
    void qq();
};

#endif // SETTINGS_H
