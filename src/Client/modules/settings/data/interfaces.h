#ifndef INTERFACE_H
#define INTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>

class Interfaces : public QObject
{
    Q_OBJECT
private:
    QSettings setting;

public:
    Interfaces() : setting(){}

    QString language;
    QString style;
    QString font;
    int volume;
    QString dateTimeFormat;
    QString firstWeekDay;
    float timeZone;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, Interfaces &s);
    friend QDataStream &operator >> (QDataStream &d, Interfaces &s);

signals:
    void saveSetting();
};

#endif // INTERFACE_H
