#include "interfaces.h"

void Interfaces::setDefault()
{
    language = "rus";
    style = "Breeze";
    volume = 0;
    dateTimeFormat = "hh:MM dd.mm.yyyy";
    firstWeekDay = "mo";
    timeZone = 2;

    this->applySetting();
}

void Interfaces::applySetting()
{
    setting.setValue("Interface/language", language);
    setting.setValue("Interface/style", style);
    setting.setValue("Interface/volume", volume);
    setting.setValue("Interface/dateTimeFormat", dateTimeFormat);
    setting.setValue("Interface/firstWeekDay", firstWeekDay);
    setting.setValue("Interface/timeZone", timeZone);
    setting.setValue("Interface/font", font);

    emit saveSetting();
}

QDataStream &operator << (QDataStream &d, Interfaces &s)
{
    d << s.language << s.style << s.volume << s.dateTimeFormat
      << s.firstWeekDay << s.timeZone << s.font;
    return d;
}

QDataStream &operator >> (QDataStream &d, Interfaces &s)
{
    d >> s.language >> s.style >> s.volume >> s.dateTimeFormat
      >> s.firstWeekDay >> s.timeZone >> s.font;
    return d;
}
