#ifndef HOTKEY_H
#define HOTKEY_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>

class HotKey : public QObject
{
    Q_OBJECT
private:
    QSettings setting;

public:
    HotKey() : setting(){}

    QString openHome;
    QString openSetting;
    QString closeApp;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, HotKey &s);
    friend QDataStream &operator >> (QDataStream &d, HotKey &s);

signals:
    void saveSetting();
};

#endif // HOTKEY_H
