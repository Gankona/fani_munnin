#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QSize>
#include <QtCore/QPoint>
#include <QtCore/QDataStream>

class Graphic : public QObject
{
    Q_OBJECT
private:
    QSettings setting;

public:
    Graphic() : setting(){}

    bool isCurrentVisibleMenu;
    bool isCurrentVisibleWindow;
    QSize currentSize;
    QPoint currentPos;

    bool isFixedSizeWindow;
    bool isChangeSizeRateably;
    bool isMenuVisible;
    bool isAlwaysShowWindow;
    bool isFloatLoginWindow;
    bool isFixedStartWindowSize;
    float rateablyWindowScretch;
    QSize startFixedWindowSize;
    QSize minimumSize;

    void setDefault();
    void applySetting();
    void saveSettingFromSetting();

    friend QDataStream &operator << (QDataStream &d, Graphic &s);
    friend QDataStream &operator >> (QDataStream &d, Graphic &s);

signals:
    void saveSetting();
};

#endif // GRAPHIC_H
