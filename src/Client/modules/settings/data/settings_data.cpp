#include "settings_data.h"


Settings_data::Settings_data() : setting()
{
    graphic = new Graphic;
    hotKey = new HotKey;
    interFace = new Interfaces;
    server = new Server;

    this->setDefault();
    graphic->applySetting();
    hotKey->applySetting();
    interFace->applySetting();
    server->applySetting();

    dir.setPath(setting.value("Path/dataPath", QDir::home().absolutePath()).toString());

    QObject::connect(graphic,   SIGNAL(saveSetting()), this, SLOT(saveSetting()));
    QObject::connect(hotKey,    SIGNAL(saveSetting()), this, SLOT(saveSetting()));
    QObject::connect(interFace, SIGNAL(saveSetting()), this, SLOT(saveSetting()));
    QObject::connect(server,    SIGNAL(saveSetting()), this, SLOT(saveSetting()));
}

void Settings_data::setDefault()
{
    graphic->setDefault();
    hotKey->setDefault();
    interFace->setDefault();
    server->setDefault();
    emit updateSetting();
}

void Settings_data::createSetting(QString login)
{
    qDebug() << login << "creating data login";
    currentLogin = login;
    QDir dirLogin = dir;
    if (!dirLogin.cd(currentLogin)){
        dirLogin.mkdir(currentLogin);
        dirLogin.cd(currentLogin);
    }
    QString str = dirLogin.absolutePath() + "/" + currentLogin + ".fms";
    QFile file;
    file.setFileName(str);
    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream;
        stream.setDevice(&file);
        stream >> *this;
        file.close();
        qDebug() << graphic->currentSize << "QWE";
    }
    else {
        this->setDefault();
        this->saveSetting();
        this->createSetting(currentLogin);
    }
}

void Settings_data::saveSetting()
{
    QDir dirLogin = dir;
    if (!dirLogin.cd(currentLogin)){
        dirLogin.mkdir(currentLogin);
        dirLogin.cd(currentLogin);
    }
    QString str = dirLogin.absolutePath() + "/" + currentLogin + ".fms";
    QFile file;
    file.setFileName(str);
    file.open(QIODevice::WriteOnly);
    QDataStream stream;
    stream.setDevice(&file);
    stream << *this;
    file.close();
    emit updateSetting();
}

QDataStream &operator << (QDataStream &d, Settings_data &s)
{
    d << *s.graphic << *s.hotKey << *s.interFace << *s.server;
    return d;
}

QDataStream &operator >> (QDataStream &d, Settings_data &s)
{
    d >> *s.graphic >> *s.hotKey >> *s.interFace >> *s.server;
    s.graphic->applySetting();
    s.hotKey->applySetting();
    s.interFace->applySetting();
    s.server->applySetting();
    return d;
}

void Settings_data::updateSettingFromReg()
{
    qDebug() << setting.value("Graphic/currentSize").toSize() << "updSetFromReg1" << graphic->currentSize;
    graphic->saveSettingFromSetting();
    qDebug() << setting.value("Graphic/currentSize").toSize() << "updSetFromReg2" << graphic->currentSize;
    saveSetting();
    qDebug() << setting.value("Graphic/currentSize").toSize() << "updSetFromReg3" << graphic->currentSize;
}
