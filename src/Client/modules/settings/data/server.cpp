#include "server.h"

Server::Server() : setting()
{

}

void Server::setDefault()
{
    defaultVersionSave = "1.0";
    isAyRateably = false;

    this->applySetting();
}

void Server::applySetting()
{
    setting.setValue("Server/defaultVersionSave", defaultVersionSave);
    setting.setValue("Server/isAyRateably", isAyRateably);

    emit saveSetting();
}

QDataStream &operator << (QDataStream &d, Server &s)
{
    return d;
}

QDataStream &operator >> (QDataStream &d, Server &s)
{
    return d;
}
