#ifndef SERVER_H
#define SERVER_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>

class Server : public QObject
{
    Q_OBJECT
public:
    Server();
    QSettings setting;

    QString defaultVersionSave;
    bool isAyRateably;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, Server &s);
    friend QDataStream &operator >> (QDataStream &d, Server &s);

signals:
    void saveSetting();
};

#endif // SERVER_H
