#include "hotkey.h"

void HotKey::setDefault()
{
    openHome = "Ctrl+1";
    openSetting = "Ctrl+8";
    closeApp = "Ctrl+Q";

    this->applySetting();
}

void HotKey::applySetting()
{
    setting.setValue("HotKey/openHome", openHome);
    setting.setValue("HotKey/openSetting", openSetting);
    setting.setValue("HotKey/closeApp", closeApp);

    emit saveSetting();
}

QDataStream &operator << (QDataStream &d, HotKey &s)
{
    d << s.openHome << s.openSetting << s.closeApp;
    return d;
}

QDataStream &operator >> (QDataStream &d, HotKey &s)
{
    d >> s.openHome >>s.openSetting >> s.closeApp;
    return d;
}
