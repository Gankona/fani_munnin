#include "graphic.h"

void Graphic::setDefault()
{
    isCurrentVisibleMenu = false;
    isCurrentVisibleWindow = false;
    currentSize = QSize(600, 400);
    currentPos = QPoint(0, 0);

    isFixedSizeWindow = false;
    isChangeSizeRateably = false;
    isMenuVisible = true;
    isAlwaysShowWindow = false;
    isFloatLoginWindow = true;
    isFixedStartWindowSize = false;
    startFixedWindowSize = QSize(600, 400);
    minimumSize = QSize(400, 300);
    rateablyWindowScretch = 1.5;

    this->applySetting();
}

void Graphic::applySetting()
{
    setting.beginGroup("Graphic");
    setting.setValue("isCurrentVisibleMenu", isCurrentVisibleMenu);
    setting.setValue("isCurrentVisibleWindow", isCurrentVisibleWindow);
    setting.setValue("currentSize", currentSize);
    setting.setValue("currentPos", currentPos);

    setting.setValue("isFixedSizeWindow", isFixedSizeWindow);
    setting.setValue("isChangeSizeRateably", isChangeSizeRateably);
    setting.setValue("isMenuVisible", isMenuVisible);
    setting.setValue("isAlwaysShowWindow", isAlwaysShowWindow);
    setting.setValue("isFloatLoginWindow", isFloatLoginWindow);
    setting.setValue("isFixedStartWindowSize", isFixedStartWindowSize);
    setting.setValue("startFixedWindowSize", startFixedWindowSize);
    setting.setValue("minimumSize", minimumSize);
    setting.setValue("rateablyWindowScretch", rateablyWindowScretch);
    setting.endGroup();

    emit saveSetting();
}

void Graphic::saveSettingFromSetting()
{
    setting.beginGroup("Graphic/");
    isCurrentVisibleMenu = setting.value("isCurrentVisibleMenu").toBool();
    isCurrentVisibleWindow = setting.value("isCurrentVisibleWindow").toBool();
    currentSize = setting.value("currentSize").toSize();
    currentPos = setting.value("currentPos").toPoint();

    isFixedSizeWindow = setting.value("isFixedSizeWindow").toBool();
    isChangeSizeRateably = setting.value("isChangeSizeRateably").toBool();
    isMenuVisible = setting.value("isMenuVisible").toBool();
    isAlwaysShowWindow = setting.value("isAlwaysShowWindow").toBool();
    isFloatLoginWindow = setting.value("isFloatLoginWindow").toBool();
    isFixedStartWindowSize = setting.value("isFixedStartWindowSize").toBool();
    startFixedWindowSize = setting.value("startFixedWindowSize").toSize();
    minimumSize = setting.value("minimumSize").toSize();
    rateablyWindowScretch = setting.value("rateablyWindowScretch").toFloat();
    setting.endGroup();
}

QDataStream &operator << (QDataStream &d, Graphic &s)
{
    d << s.isCurrentVisibleMenu << s.isCurrentVisibleWindow << s.currentSize
      << s.isFixedSizeWindow << s.isChangeSizeRateably << s.isMenuVisible
      << s.isAlwaysShowWindow << s.isFloatLoginWindow << s.isFixedStartWindowSize
      << s.startFixedWindowSize << s.minimumSize
      << s.rateablyWindowScretch << s.currentPos;
    return d;
}

QDataStream &operator >> (QDataStream &d, Graphic &s)
{
    d >> s.isCurrentVisibleMenu >> s.isCurrentVisibleWindow >> s.currentSize
      >> s.isFixedSizeWindow >> s.isChangeSizeRateably >> s.isMenuVisible
      >> s.isAlwaysShowWindow >> s.isFloatLoginWindow >> s.isFixedStartWindowSize
      >> s.startFixedWindowSize >> s.minimumSize
      >> s.rateablyWindowScretch >> s.currentPos;
    return d;
}
