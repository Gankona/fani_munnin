#ifndef INTERFACEVIEW_H
#define INTERFACEVIEW_H

#include "mmarginwidget.h"
#include "data/interfaces.h"
#include "mhlistwidget.h"
#include "styleexamplewidget.h"

#include <QtGui/QKeyEvent>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QSlider>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStyleFactory>
#include <QDebug>

class InterfaceView : public Mmarginwidget
{
    Q_OBJECT
private:
    Interfaces *data;

    QScrollArea *area;
    QGroupBox *groupBox;

    QMap <QString, QLabel*> mapLabel;
    QMap <QString, MHListWidget*> mapButton;

    //language setting
    QLabel *languageLabel;

    //appearence setting
    QLabel *facadeLabel;
    QMap <QString, StyleExampleWidget*> styleList;

    //volume setting
    QLabel *volumeLabel;
    QSlider *volumeSlider;

    //date setting
    QLabel *dateLabel;
    QLineEdit *dateTimeFormatEdit;

    //down buttons
    QHBoxLayout *hBox;
    QFrame *downFrame;
    QPushButton *saveButton;
    QPushButton *defaultButton;
    QPushButton *backButton;

    int currentRow;

    inline void createSettings();
    inline void createDownButton();
    void setSettingFromData();

public:
    InterfaceView(Interfaces *data, QWidget *parent = nullptr);
    void resizeContent() Q_DECL_OVERRIDE;

private slots:
    void saveSetting();
    void setDefault();
    void chooseStyle();

signals:
    void goHome();
};

#endif // INTERFACEVIEW_H
