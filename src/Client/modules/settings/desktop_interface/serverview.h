#ifndef SERVERVIEW_H
#define SERVERVIEW_H

#include "mmarginwidget.h"
#include "data/server.h"

#include <QtGui/QKeyEvent>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QDebug>

class ServerView : public Mmarginwidget
{
    Q_OBJECT
private:
    Server *data;

    QScrollArea *area;
    QGroupBox *groupBox;
    QLabel *titleLabel;
    QMap <QString, QLabel*> mapLabel;
    QMap <QString, QPushButton*> mapButton;
    QHBoxLayout *hBox;
    QFrame *downFrame;
    QPushButton *saveButton;
    QPushButton *defaultButton;
    QPushButton *backButton;

    QString buttonTextBuffer;
    QPushButton *buttonBuffer;

    int currentRow;

    inline void createOneHotKey(QString name, QString nameKey, QString keyCombo);
    inline void createHotKey();
    inline void createDownButton();

public:
    ServerView(Server *data, QWidget *parent = nullptr);
    void resizeContent() Q_DECL_OVERRIDE;

private slots:
    void clickToChange();
    void saveSetting();
    void setDefault();

signals:
    void goHome();
};

#endif // SERVERVIEW_H
