#include "view.h"

View::View(Settings_data *data, QWidget *parent)
    : MContentWidget(parent), data(data), settings("Gankona", "Fani Munnin")
{
    graphic = nullptr;
    hotKey = nullptr;
    interfaces = nullptr;
    server = nullptr;
    currentWidget = -1;
    setChooseWidget(0);
    QString styleName = settings.value("Interface/style", "default-winter").toString();
    sideBar->setNewButton(true, QIcon(":/settings.png"), tr("Графика"));
    sideBar->setNewButton(true, QIcon(":/settings.png"), tr("Гарячие клавиши"));
    sideBar->setNewButton(true, QIcon(":/settings.png"), tr("Интерфейс"));
    sideBar->setNewButton(true, QIcon(":/settings.png"), tr("Сервер"));

    QObject::connect(sideBar, SIGNAL(setCurrentWidget(int)), this, SLOT(setChooseWidget(int)));
}

void View::setChooseWidget(int i)
{
    if (currentWidget == i)
        return;
    if (graphic != nullptr){
        delete graphic;
        graphic = nullptr;
    }
    if (interfaces != nullptr){
        delete interfaces;
        interfaces = nullptr;
    }
    if (hotKey != nullptr){
        delete hotKey;
        hotKey = nullptr;
    }
    if (server != nullptr){
        delete server;
        server = nullptr;
    }

    currentWidget = i;
    titleLabel->clear();
    switch (currentWidget) {
    case 0:
        graphic = new GraphicView(data->graphic, contentWidget);
        graphic->setFixedSize(contentWidget->size());
        titleLabel->setText(tr("Настройки графики"));
        QObject::connect(graphic, SIGNAL(goHome()), this, SIGNAL(goHome()));
        break;
    case 1:
        hotKey = new HotKeyView(data->hotKey, contentWidget);
        hotKey->setFixedSize(contentWidget->size());
        titleLabel->setText(tr("Настройки гарячих клавиш"));
        QObject::connect(hotKey, SIGNAL(goHome()), this, SIGNAL(goHome()));
        break;
    case 2:
        interfaces = new InterfaceView(data->interFace, contentWidget);
        interfaces->setFixedSize(contentWidget->size());
        titleLabel->setText(tr("Настройки интерфейса"));
        QObject::connect(interfaces, SIGNAL(goHome()), this, SIGNAL(goHome()));
        break;
    case 3:
        server = new ServerView(data->server, contentWidget);
        server->setFixedSize(contentWidget->size());
        titleLabel->setText(tr("Настройки сервера"));
        QObject::connect(server, SIGNAL(goHome()), this, SIGNAL(goHome()));
        break;
    default:
        setChooseWidget(0);
    }
    this->setFixedSize(this->size());
}
