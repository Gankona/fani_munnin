#include "styleexamplewidget.h"

StyleExampleWidget::StyleExampleWidget(QString title, QWidget *parent, QSize size)
    : QFrame(parent)
{
    this->setFixedSize(size);
    titleLabel = new QLabel(title, this);
    lineEdit = new QLineEdit(this);
    button = new QPushButton(tr("Выбрать"), this);
    textLabel = new QLabel("Логические найдены ходы,\n"
                           "Все лабиринты, пропасти, пустоты\n"
                           "Преодолела мысль – и вот плоды\n"
                           "Такой тяжелой, каторжной работы.\n", this);
    textLabel->setWordWrap(true);
    textLabel->setAlignment(Qt::AlignLeft|Qt::AlignTop);
    titleLabel->setAlignment(Qt::AlignCenter);
    panelBar = new MSidePanelBar(this);
    panelBar->setNewButton(true, QIcon(":/settings.png"), "");
    panelBar->setNewButton(true, QIcon(":/settings.png"), "");
    panelBar->setNewButton(true, QIcon(":/settings.png"), "");
    this->resizeEvent(nullptr);
    QFont font;
    font.setPixelSize(9);
    textLabel->setFont(font);
    this->setFrameStyle(QFrame::Panel | QFrame::Plain);
    this->setLineWidth(1);
    QObject::connect(button, SIGNAL(clicked(bool)), SIGNAL(chooseStyle()));
}

bool StyleExampleWidget::isChoose()
{
    return chooseStatus;
}

void StyleExampleWidget::setChoose(bool b)
{
    chooseStatus = b;
    b ? button->setIcon(QIcon("://yes.png"))
      : button->setIcon(QIcon("://no.png"));
}

void StyleExampleWidget::resizeEvent(QResizeEvent *e)
{
    QSize size;
    e == nullptr ? size = this->size() : size = e->size();
    //set Size to component
    titleLabel->setFixedSize(size.width() - 4, 25);
    panelBar->setFixedHeight(size.height() - titleLabel->height());
    button->setFixedSize(size.width() - panelBar->width() - 4, 25);
    lineEdit->setFixedSize(size.width() - panelBar->width() - 4, 30);
    textLabel->setFixedSize(size - QSize(panelBar->width(),
                                         button->height()
                                         + lineEdit->height()
                                         + titleLabel->height()));

    //move component under widget
    panelBar->move(2, 2);
    textLabel->move(panelBar->width() + 4, 2);
    titleLabel->move(2, panelBar->height());
    lineEdit->move(panelBar->width() + 2, textLabel->height());
    button->move(panelBar->width() + 2, textLabel->height() + lineEdit->height());
}
