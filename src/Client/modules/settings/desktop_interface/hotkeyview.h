#ifndef HOTKEYVIEW_H
#define HOTKEYVIEW_H

#include "mmarginwidget.h"
#include "data/hotkey.h"
#include "mkey.h"

#include <QtCore/QMimeType>

#include <QtGui/QKeyEvent>
#include <QtWidgets/QApplication>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QDebug>

class HotKeyView : public Mmarginwidget
{
    Q_OBJECT
private:
    HotKey *data;
    Mkey *mkey;

    QScrollArea *area;
    QGroupBox *groupBox;
    QLabel *titleLabel;
    QMap <QString, QLabel*> mapLabel;
    QMap <QString, QPushButton*> mapButton;
    QHBoxLayout *hBox;
    QFrame *downFrame;
    QPushButton *saveButton;
    QPushButton *defaultButton;
    QPushButton *backButton;

    QString buttonTextBuffer;
    QPushButton *buttonBuffer;

    int currentRow;

    inline void createOneHotKey(QString name, QString nameKey, QString keyCombo);
    inline void createHotKey();
    inline void createDownButton();
    void keyPressEvent(QKeyEvent *e);

public:
    HotKeyView(HotKey *data, QWidget *parent = nullptr);
    void resizeContent() Q_DECL_OVERRIDE;

private slots:
    void clickToChange();
    void saveSetting();
    void setDefault();

signals:
    void goHome();
};

#endif // HOTKEYVIEW_H
