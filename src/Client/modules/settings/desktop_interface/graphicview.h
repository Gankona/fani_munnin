#ifndef GRAPHICVIEW_H
#define GRAPHICVIEW_H

#include "mswitcherpos.h"
#include "mmarginwidget.h"
#include "data/graphic.h"

#include <QtGui/QKeyEvent>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QApplication>
#include <QDebug>

class GraphicView : public Mmarginwidget
{
    Q_OBJECT
private:
    Graphic *data;

    QScrollArea *area;
    QGroupBox *groupBox;

    QMap <QString, QLabel*> mapLabel;
    QMap <QString, MSwitcherPos*> mapButton;

    QHBoxLayout *hBox;
    QFrame *downFrame;
    QPushButton *saveButton;
    QPushButton *defaultButton;
    QPushButton *backButton;

    QSlider *startFixedWindowSizeX;
    QSlider *startFixedWindowSizeY;
    QSlider *minimumSizeX;
    QSlider *minimumSizeY;
    QSlider *rateablySize;

    QLabel *rateablySizeLabel;
    QLabel *startFixedWindowSizeLabel;
    QLabel *minimumSizeLabel;
    QLabel *startFixedShowLabelX;
    QLabel *startFixedShowLabelY;
    QLabel *startFixedShowLabelVS;
    QLabel *minimumSizeShowLabelX;
    QLabel *minimumSizeShowLabelY;
    QLabel *minimumSizeShowLabelVS;
    QLabel *rateablySizeShowLabel;

    int currentRow;
    QSize tempSize;

    inline void createOnePoint(QString name, QString nameKey, bool status);
    inline void createPoint();
    inline void createDownButton();
    inline void createSlider();

public:
    GraphicView(Graphic *data, QWidget *parent = nullptr);
    void resizeContent() Q_DECL_OVERRIDE;

private slots:
    void saveSetting();
    void setDefault();
    void updatePoint();

signals:
    void goHome();
};

#endif // GRAPHICVIEW_H
