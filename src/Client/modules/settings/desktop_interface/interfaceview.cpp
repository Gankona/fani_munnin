#include "interfaceview.h"

InterfaceView::InterfaceView(Interfaces *data, QWidget *parent)
    : Mmarginwidget(parent), data(data)
{
    mapButton.clear();
    mapLabel.clear();

    groupBox = new QGroupBox;
    area = new QScrollArea(this->content);
    area->show();
    area->setWidget(groupBox);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    createSettings();
    createDownButton();

    this->show();
}

void InterfaceView::createDownButton()
{
    hBox = new QHBoxLayout;
    saveButton = new QPushButton(tr("сохранить"), groupBox);
    defaultButton = new QPushButton(tr("по умолчанию"), groupBox);
    backButton = new QPushButton(tr("назад"), groupBox);
    saveButton->setFixedHeight(30);
    defaultButton->setFixedHeight(30);
    backButton->setFixedHeight(30);
    hBox->addWidget(new QFrame);
    hBox->addWidget(saveButton);
    hBox->addWidget(defaultButton);
    hBox->addWidget(backButton);
    downFrame = new QFrame(groupBox);
    downFrame->setLayout(hBox);

    QObject::connect(backButton, SIGNAL(clicked(bool)), this, SIGNAL(goHome()));
    QObject::connect(defaultButton, SIGNAL(clicked(bool)), this, SLOT(setDefault()));
    QObject::connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(saveSetting()));
}

void InterfaceView::createSettings()
{
    qDebug() << 1;
    QString key = "language";
    languageLabel = new QLabel(tr("Настройка языка интерфейса") + ":", groupBox);
    mapLabel.insert(key, new QLabel(tr("Выбор языка интерфейса"), groupBox));
    mapButton.insert(key, new MHListWidget(groupBox, false, QSize(100, 25)));
    mapButton.value(key)->comboBox()->addItems(QStringList{"EN", "FR", "RU", "UA"});

    facadeLabel = new QLabel(tr("Настройка стиля интерфейса") + ":", groupBox);
    //style
    foreach (QString key, QStyleFactory::keys()){
        styleList.insert(key, new StyleExampleWidget(key, groupBox, QSize(150, 200)));
        styleList.value(key)->setStyle(QStyleFactory::create(key));
        QObject::connect(styleList.value(key), SIGNAL(chooseStyle()), this, SLOT(chooseStyle()));
    }
    qDebug() << "Styles: " << styleList.keys();

    //font
    key = "font";
    mapLabel.insert(key, new QLabel(tr("Выбор шрифта приложения"), groupBox));
    mapButton.insert(key, new MHListWidget(groupBox, true, QSize(200, 25)));
    static_cast<QFontComboBox*>(mapButton.value(key)->comboBox())->setFontFilters(QFontComboBox::AllFonts);

    key = "volume";
    volumeLabel = new QLabel(tr("Настройка звука") + ":", groupBox);
    mapLabel.insert(key, new QLabel(tr("Настройка громкости"), groupBox));
    volumeSlider = new QSlider(Qt::Orientation::Horizontal, groupBox);

    dateLabel = new QLabel(tr("Настройка времени") + ":", groupBox);
    key = "dateTimeFormat";
    mapLabel.insert(key, new QLabel(tr("Формат вывода времени"), groupBox));
    dateTimeFormatEdit = new QLineEdit(data->dateTimeFormat, groupBox);

    key = "firstWeekDay";
    mapLabel.insert(key, new QLabel(tr("День начала недели"), groupBox));
    mapButton.insert(key, new MHListWidget(groupBox, false));
    mapButton.value(key)->comboBox()->addItems(QStringList{tr("Пн"), tr("Вт"), tr("Ср"),
                                                   tr("Чт"), tr("Пт"), tr("Сб"), tr("Вс")});
    key = "timeZone";
    mapLabel.insert(key, new QLabel(tr("Часовой пояс"), groupBox));
    mapButton.insert(key, new MHListWidget(groupBox, false));
    for (int i = -12; i <= 12; i++)
        mapButton.value(key)->comboBox()->addItem(QString::number(i));

    setSettingFromData();
}

void InterfaceView::resizeContent()
{
    area->setFixedSize(content->size());
    groupBox->setFixedWidth(content->width());
    int i(5);
    QString key = "language";
    //Language
    languageLabel->setFixedSize(groupBox->width()-10, 30);
    languageLabel->move(5, i);
    i += 32;
    mapLabel.value(key)->setFixedSize(groupBox->width() - (mapButton.value(key)->width() + 10),
                                             mapButton.value(key)->height());
    mapLabel.value(key)->move(5, i);
    mapButton.value(key)->move(groupBox->width() - (mapButton.value(key)->width() + 5), i);
    i += 2 + mapButton.value(key)->height();
    i += 50;

    //facade
    facadeLabel->setFixedSize(groupBox->width()-10, 30);
    facadeLabel->move(5, i);
    i += 32;

    //style
    int column = (groupBox->width() - 10)/155;
    int currentStylePaint = 0;
    if (column == 0)
        column++;
    foreach (QString s, styleList.keys()){
        styleList.value(s)->setFixedWidth((groupBox->width() - (column + 1) * 10)/column);
        styleList.value(s)->move((styleList.value(s)->width() + 5)*(currentStylePaint%column) + 5,
                             i + (styleList.value(s)->height()+ 5)*(currentStylePaint/column) + 5);
        currentStylePaint++;
    }
    i += (((styleList.keys().length()-1)/column)+1)*205 + 5;

    //font
    key = "font";
    mapLabel.value(key)->setFixedSize(groupBox->width() - mapButton.value(key)->width() - 10,
                                             mapButton.value(key)->height());
    mapLabel.value(key)->move(5, i);
    mapButton.value(key)->move(groupBox->width() - mapButton.value("font")->width() - 5, i);
    i += 2 + mapButton.value(key)->height();
    i += 50;

    //volume
    key = "volume";
    volumeLabel->setFixedSize(groupBox->width() - 10, 30);
    volumeLabel->move(5, i);
    i += 2 + volumeLabel->height();
    mapLabel.value(key)->setFixedSize((groupBox->width()-10)/2, 30);
    mapLabel.value(key)->move(5, i);
    volumeSlider->setFixedSize((groupBox->width()-10)/2, 30);
    volumeSlider->move(groupBox->width()/2, i);
    i += 2 + volumeSlider->height();
    i += 50;

    //time
    key = "dateTimeFormat";
    dateLabel->setFixedSize(groupBox->width() - 10, 30);
    dateLabel->move(5, i);
    i += 2 + dateLabel->height();
    dateTimeFormatEdit->setFixedSize(175, 30);
    dateTimeFormatEdit->move(groupBox->width() - (dateTimeFormatEdit->width() + 5), i);
    mapLabel.value(key)->setFixedSize(groupBox->width() - (dateTimeFormatEdit->width() + 10),
                                      dateTimeFormatEdit->height());
    mapLabel.value(key)->move(5, i);
    i += 2 + dateTimeFormatEdit->height();

    key = "firstWeekDay";
    mapLabel.value(key)->setFixedSize(groupBox->width() - (mapButton.value(key)->width() + 10),
                                      mapButton.value(key)->height());
    mapLabel.value(key)->move(5, i);
    mapButton.value(key)->move(mapLabel.value(key)->width() + 5, i);
    i += 2 + mapLabel.value(key)->height();

    key = "timeZone";
    mapLabel.value(key)->setFixedSize(groupBox->width() - (mapButton.value(key)->width() + 10),
                                      mapButton.value(key)->height());
    mapLabel.value(key)->move(5, i);
    mapButton.value(key)->move(mapLabel.value(key)->width() + 5, i);
    i += 2 + mapLabel.value(key)->height();

    //down button
    i += 10;
    downFrame->setFixedSize(groupBox->width(), 35);
    downFrame->move(0, i);
    groupBox->setFixedHeight(i + downFrame->height() + 10);
}

void InterfaceView::saveSetting()
{
    data->dateTimeFormat = dateTimeFormatEdit->text();
    data->firstWeekDay = mapButton.value("firstWeekDay")->comboBox()->currentText();
    data->font = mapButton.value("font")->comboBox()->currentText();
    data->language = mapButton.value("language")->comboBox()->currentText();
    data->timeZone = mapButton.value("timeZone")->comboBox()->currentText().toFloat();
    data->volume = volumeSlider->value();
    foreach (QString key, styleList.keys())
        if (styleList.value(key)->isChoose())
            data->style = key;

    data->applySetting();
}

void InterfaceView::setDefault()
{
    data->setDefault();
    setSettingFromData();
}

void InterfaceView::setSettingFromData(){
    dateTimeFormatEdit->setText(data->dateTimeFormat);
    mapButton.value("firstWeekDay")->comboBox()->setCurrentText(data->firstWeekDay);
    mapButton.value("font")->comboBox()->setCurrentText(data->font);
    mapButton.value("language")->comboBox()->setCurrentText(data->language);
    mapButton.value("timeZone")->comboBox()->setCurrentText(QString::number(data->timeZone));
    volumeSlider->setValue(data->volume);
    styleList.value(data->style)->chooseStyle();
}

void InterfaceView::chooseStyle()
{
    foreach (QString key, styleList.keys())
        styleList.value(key) == sender() ? styleList.value(key)->setChoose(true)
                                         : styleList.value(key)->setChoose(false);
}
