#include "serverview.h"

ServerView::ServerView(Server *data, QWidget *parent)
    : Mmarginwidget(parent), data(data)
{
    buttonBuffer = nullptr;
    buttonTextBuffer = "";
    mapButton.clear();
    mapLabel.clear();

    groupBox = new QGroupBox;
    area = new QScrollArea(this->content);
    area->show();
    area->setWidget(groupBox);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    createHotKey();
    createDownButton();

    this->show();
}

void ServerView::createDownButton()
{
    hBox = new QHBoxLayout;
    saveButton = new QPushButton(tr("сохранить"), groupBox);
    defaultButton = new QPushButton(tr("по умолчанию"), groupBox);
    backButton = new QPushButton(tr("назад"), groupBox);
    saveButton->setFixedHeight(30);
    defaultButton->setFixedHeight(30);
    backButton->setFixedHeight(30);
    hBox->addWidget(new QFrame);
    hBox->addWidget(saveButton);
    hBox->addWidget(defaultButton);
    hBox->addWidget(backButton);
    downFrame = new QFrame(groupBox);
    downFrame->setLayout(hBox);

    QObject::connect(backButton, SIGNAL(clicked(bool)), this, SIGNAL(goHome()));
    QObject::connect(defaultButton, SIGNAL(clicked(bool)), this, SLOT(setDefault()));
    QObject::connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(saveSetting()));
}

void ServerView::createOneHotKey(QString name, QString nameKey, QString keyCombo)
{
    mapLabel.insert(nameKey, new QLabel(name, groupBox));
    mapButton.insert(nameKey, new QPushButton(keyCombo, groupBox));
    mapButton.value(nameKey)->setCheckable(true);
    QObject::connect(mapButton.value(nameKey), SIGNAL(clicked(bool)), this, SLOT(clickToChange()));
}

void ServerView::createHotKey()
{
//    createOneHotKey(tr("Открыть дом"), "openHome",data->openHome);
//    createOneHotKey(tr("Открыть настройки"), "openSetting", data->openSetting);
//    createOneHotKey(tr("Закрыть приложение"), "closeApp", data->closeApp);
}

void ServerView::resizeContent()
{
    area->setFixedSize(content->size());
    groupBox->setFixedWidth(content->width());
    int i(0);
    foreach (QString k, mapButton.keys()){
        mapButton.value(k)->setFixedSize(135, 30);
        mapButton.value(k)->move(groupBox->width() - 140, i * 31);
        i++;
    }
    i = 0;
    foreach (QString k, mapLabel.keys()){
        mapLabel.value(k)->setFixedSize(groupBox->width() - 150, 30);
        mapLabel.value(k)->move(5, i * 31);
        i++;
    }
    downFrame->setFixedSize(groupBox->width(), 35);
    downFrame->move(0, i * 31 + 31);
    groupBox->setFixedHeight(i * 31 + 85);
}

void ServerView::saveSetting()
{
    if (buttonTextBuffer != "")
        if (buttonBuffer != nullptr)
            buttonBuffer->setText(buttonTextBuffer);
//    data->openHome = mapButton.value("openHome")->text();
//    data->openSetting = mapButton.value("openSetting")->text();
//    data->closeApp = mapButton.value("closeApp")->text();
    data->applySetting();
}

void ServerView::setDefault()
{
    data->setDefault();
//    mapButton.value("openHome")->setText(data->openHome);
//    mapButton.value("openSetting")->setText(data->openSetting);
//    mapButton.value("closeApp")->setText(data->closeApp);
}

void ServerView::clickToChange()
{
    if (buttonBuffer != nullptr)
        buttonBuffer->setText(buttonTextBuffer);
    buttonBuffer = ((QPushButton*)sender());
    buttonTextBuffer = ((QPushButton*)sender())->text();
    foreach (QString k, mapButton.keys())
        if (mapButton.value(k)->isChecked())
            mapButton.value(k)->setChecked(false);
    ((QPushButton*)sender())->setChecked(true);
    ((QPushButton*)sender())->setText("");
}
