#ifndef STYLEEXAMPLEWIDGET_H
#define STYLEEXAMPLEWIDGET_H

#include "msidepanelbar.h"

#include <QtGui/QFont>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

class StyleExampleWidget : public QFrame
{
    Q_OBJECT
private:
    MSidePanelBar *panelBar;
    QLabel *textLabel;
    QLineEdit *lineEdit;
    QPushButton *button;
    QLabel *titleLabel;
    bool chooseStatus;

protected:
    void resizeEvent(QResizeEvent *e);

public:
    explicit StyleExampleWidget(QString title,
                                QWidget *parent = 0,
                                QSize size = QSize(150, 200));

    bool isChoose();
    void setChoose(bool b);

signals:
    void chooseStyle();
};

#endif // STYLEEXAMPLEWIDGET_H
