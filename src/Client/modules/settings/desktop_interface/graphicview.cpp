#include "graphicview.h"

GraphicView::GraphicView(Graphic *data, QWidget *parent)
    : Mmarginwidget(parent), data(data)
{
    mapButton.clear();
    mapLabel.clear();

    groupBox = new QGroupBox;
    area = new QScrollArea(this->content);
    area->show();
    area->setWidget(groupBox);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    createPoint();
    createSlider();
    createDownButton();
    tempSize = data->currentSize;

    this->show();
}

void GraphicView::createDownButton()
{
    hBox = new QHBoxLayout;
    saveButton = new QPushButton(tr("сохранить"), groupBox);
    defaultButton = new QPushButton(tr("по умолчанию"), groupBox);
    backButton = new QPushButton(tr("назад"), groupBox);
    saveButton->setFixedHeight(30);
    defaultButton->setFixedHeight(30);
    backButton->setFixedHeight(30);
    hBox->addWidget(new QFrame);
    hBox->addWidget(saveButton);
    hBox->addWidget(defaultButton);
    hBox->addWidget(backButton);
    downFrame = new QFrame(groupBox);
    downFrame->setLayout(hBox);

    QObject::connect(backButton, SIGNAL(clicked(bool)), this, SIGNAL(goHome()));
    QObject::connect(defaultButton, SIGNAL(clicked(bool)), this, SLOT(setDefault()));
    QObject::connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(saveSetting()));
}

void GraphicView::createOnePoint(QString name, QString nameKey, bool status)
{
    mapLabel.insert(nameKey, new QLabel(name, groupBox));
    mapButton.insert(nameKey, new MSwitcherPos(groupBox));
    mapButton.value(nameKey)->setSwitcherPos(status);
    QObject::connect(mapButton.value(nameKey), SIGNAL(buttonPressed(bool)), this, SLOT(updatePoint()));
}

void GraphicView::createPoint()
{
    createOnePoint(tr("Показывать окно быстрого меню"), "isMenuVisible", data->isMenuVisible);
    createOnePoint(tr("Всегда показывать окно"), "isAlwaysShowWindow", data->isAlwaysShowWindow);
    createOnePoint(tr("Зафиксировать размер окна"), "isFixedSizeWindow", data->isFixedSizeWindow);
    createOnePoint(tr("Полупрозрачное окно авторизации"), "isFloatLoginWindow", data->isFloatLoginWindow);
    createOnePoint(tr("Изменять размер пропорционально"), "isChangeSizeRateably", data->isChangeSizeRateably);
    createOnePoint(tr("Фиксированый размер окна при запуске"), "isFixedStartWindowSize", data->isFixedStartWindowSize);
}

void GraphicView::createSlider()
{
    minimumSizeX = new QSlider(Qt::Orientation::Horizontal, groupBox);
    minimumSizeX->setRange(500, QApplication::desktop()->screen()->width());
    minimumSizeX->setValue(data->minimumSize.width());

    minimumSizeY = new QSlider(Qt::Orientation::Horizontal, groupBox);
    minimumSizeY->setRange(300, QApplication::desktop()->screen()->height());
    minimumSizeY->setValue(data->minimumSize.height());

    startFixedWindowSizeX = new QSlider(Qt::Orientation::Horizontal, groupBox);
    startFixedWindowSizeX->setRange(data->minimumSize.width(), QApplication::desktop()->screen()->width());
    startFixedWindowSizeX->setTickInterval(10);
    startFixedWindowSizeX->setValue(data->startFixedWindowSize.width());

    startFixedWindowSizeY = new QSlider(Qt::Orientation::Horizontal, groupBox);
    startFixedWindowSizeY->setRange(data->minimumSize.height(), QApplication::desktop()->screen()->width());
    startFixedWindowSizeY->setTickInterval(10);
    startFixedWindowSizeY->setValue(data->startFixedWindowSize.height());

    rateablySize = new QSlider(Qt::Orientation::Horizontal, groupBox);
    rateablySize->setRange(5, 25);
    rateablySize->setTickInterval(1);
    rateablySize->setValue(data->rateablyWindowScretch*10);
    rateablySize->setTickPosition(QSlider::TicksBelow);

    startFixedWindowSizeLabel = new QLabel(tr("Размер окна при старте"), groupBox);
    startFixedWindowSizeLabel->setAlignment(Qt::AlignLeft);
    minimumSizeLabel = new QLabel(tr("Минимальный размер окна"), groupBox);
    minimumSizeLabel->setAlignment(Qt::AlignLeft);
    rateablySizeLabel = new QLabel(tr("Соотношение сторон (x10)"), groupBox);
    rateablySizeLabel->setAlignment(Qt::AlignLeft);

    startFixedShowLabelX  = new QLabel(QString::number(data->startFixedWindowSize.width()), groupBox);
    startFixedShowLabelX->setAlignment(Qt::AlignRight);
    startFixedShowLabelY  = new QLabel(QString::number(data->startFixedWindowSize.height()), groupBox);
    startFixedShowLabelY->setAlignment(Qt::AlignLeft);
    startFixedShowLabelVS  = new QLabel("x", groupBox);
    startFixedShowLabelVS->setAlignment(Qt::AlignCenter);

    minimumSizeShowLabelX = new QLabel(QString::number(data->minimumSize.width()), groupBox);
    minimumSizeShowLabelX->setAlignment(Qt::AlignRight);
    minimumSizeShowLabelY = new QLabel(QString::number(data->minimumSize.height()), groupBox);
    minimumSizeShowLabelY->setAlignment(Qt::AlignLeft);
    minimumSizeShowLabelVS = new QLabel("x", groupBox);
    minimumSizeShowLabelVS->setAlignment(Qt::AlignCenter);

    rateablySizeShowLabel = new QLabel(QString::number(data->rateablyWindowScretch), groupBox);
    rateablySizeShowLabel->setAlignment(Qt::AlignRight);

    QObject::connect(rateablySize, SIGNAL(valueChanged(int)), rateablySizeShowLabel, SLOT(setNum(int)));
    QObject::connect(minimumSizeX, SIGNAL(valueChanged(int)), minimumSizeShowLabelX, SLOT(setNum(int)));
    QObject::connect(minimumSizeY, SIGNAL(valueChanged(int)), minimumSizeShowLabelY, SLOT(setNum(int)));

    QObject::connect(startFixedWindowSizeX, SIGNAL(valueChanged(int)), startFixedShowLabelX, SLOT(setNum(int)));
    QObject::connect(startFixedWindowSizeY, SIGNAL(valueChanged(int)), startFixedShowLabelY, SLOT(setNum(int)));

    QObject::connect(rateablySize, SIGNAL(valueChanged(int)), this, SLOT(updatePoint()));
    QObject::connect(minimumSizeX, SIGNAL(valueChanged(int)), this, SLOT(updatePoint()));
    QObject::connect(minimumSizeY, SIGNAL(valueChanged(int)), this, SLOT(updatePoint()));

    QObject::connect(startFixedWindowSizeX, SIGNAL(valueChanged(int)), this, SLOT(updatePoint()));
    QObject::connect(startFixedWindowSizeY, SIGNAL(valueChanged(int)), this, SLOT(updatePoint()));
}

void GraphicView::resizeContent()
{
    area->setFixedSize(content->size());
    groupBox->setFixedWidth(content->width());
    int i(0);
    foreach (QString k, mapLabel.keys()){
        mapLabel.value(k)->setFixedSize(groupBox->width() - 15 - mapButton.value(k)->width(), 30);
        mapLabel.value(k)->move(5, i*32);
        mapButton.value(k)->move(10 + mapLabel.value(k)->width(), i * 32+2);
        i++;
    }

    minimumSizeLabel->setFixedSize(groupBox->width()/2 - 5, 30);
    minimumSizeLabel->move(5, i*32);
    minimumSizeShowLabelX->setFixedSize(55, 30);
    minimumSizeShowLabelX->move(minimumSizeLabel->width() + 5, i*32);
    minimumSizeShowLabelVS->setFixedSize(10, 30);
    minimumSizeShowLabelVS->move(minimumSizeLabel->width() + 60, i*32);
    minimumSizeShowLabelY->setFixedSize(55, 30);
    minimumSizeShowLabelY->move(minimumSizeLabel->width() + 70, i*32);
    minimumSizeX->move(120 + minimumSizeLabel->width() + 5, i*32);

    if (mapButton.value("isChangeSizeRateably")->isTrueSwitchPos()){
        minimumSizeX->setFixedSize(groupBox->width()/2 - 120, 30);
        minimumSizeY->setVisible(false);
        i++;
        rateablySize->setVisible(true);
        rateablySizeLabel->setVisible(true);
        rateablySizeShowLabel->setVisible(true);
        rateablySizeLabel->setFixedSize(groupBox->width()/2 - 5, 30);
        rateablySizeLabel->move(5, i*32);
        rateablySizeShowLabel->setFixedSize(50, 30);
        rateablySizeShowLabel->move(5 + rateablySizeLabel->width(), i*32);
        rateablySize->setFixedSize(groupBox->width() - (5+rateablySizeLabel->width()+rateablySizeShowLabel->width()), 30);
        rateablySize->move(5 + rateablySizeLabel->width() + rateablySizeShowLabel->width(), i*32);
    }
    else {
        minimumSizeX->setFixedSize((groupBox->width()/2 - 120) / 2, 30);
        minimumSizeY->setFixedSize(minimumSizeX->size());
        minimumSizeY->move(120 + minimumSizeLabel->width() + minimumSizeX->width() + 5, i*32);
        minimumSizeY->setVisible(true);
        rateablySize->setVisible(false);
        rateablySizeLabel->setVisible(false);
        rateablySizeShowLabel->setVisible(false);
    }

    i++;
    if (mapButton.value("isFixedStartWindowSize")->isTrueSwitchPos()){
        startFixedShowLabelX->setVisible(true);
        startFixedShowLabelY->setVisible(true);
        startFixedShowLabelVS->setVisible(true);
        startFixedWindowSizeLabel->setVisible(true);
        startFixedWindowSizeX->setVisible(true);
        startFixedWindowSizeY->setVisible(true);

        startFixedWindowSizeLabel->setFixedSize(groupBox->width()/2 - 5, 30);
        startFixedWindowSizeLabel->move(5, i*32);
        startFixedShowLabelX->setFixedSize(55, 30);
        startFixedShowLabelX->move(minimumSizeLabel->width() + 5, i*32);
        startFixedShowLabelVS->setFixedSize(10, 30);
        startFixedShowLabelVS->move(minimumSizeLabel->width() + 60, i*32);
        startFixedShowLabelY->setFixedSize(55, 30);
        startFixedShowLabelY->move(minimumSizeLabel->width() + 70, i*32);
        if (mapButton.value("isChangeSizeRateably")->isTrueSwitchPos()){
            startFixedWindowSizeX->setFixedSize(groupBox->width()/2 - 120, 30);
            startFixedWindowSizeY->setVisible(false);
        }
        else {
            startFixedWindowSizeX->setFixedSize((groupBox->width()/2 - 120) / 2, 30);
            startFixedWindowSizeY->setFixedSize(minimumSizeX->size());
            startFixedWindowSizeY->move(120 + startFixedWindowSizeLabel->width() + startFixedWindowSizeX->width() + 5, i*32);
            startFixedWindowSizeY->setVisible(true);
        }
        startFixedWindowSizeX->move(120 + minimumSizeLabel->width() + 5, i*32);
        i++;
    }
    else {
        startFixedShowLabelX->setVisible(false);
        startFixedShowLabelY->setVisible(false);
        startFixedShowLabelVS->setVisible(false);
        startFixedWindowSizeLabel->setVisible(false);
        startFixedWindowSizeX->setVisible(false);
        startFixedWindowSizeY->setVisible(false);
    }

    downFrame->setFixedSize(groupBox->width(), 35);
    downFrame->move(0, i * 31 + 31);
    groupBox->setFixedHeight(i * 31 + 100);
}

void GraphicView::saveSetting()
{
    data->isMenuVisible = mapButton.value("isMenuVisible")->isTrueSwitchPos();
    data->isFixedSizeWindow = mapButton.value("isFixedSizeWindow")->isTrueSwitchPos();
    data->isAlwaysShowWindow = mapButton.value("isAlwaysShowWindow")->isTrueSwitchPos();
    data->isFloatLoginWindow = mapButton.value("isFloatLoginWindow")->isTrueSwitchPos();
    data->isChangeSizeRateably = mapButton.value("isChangeSizeRateably")->isTrueSwitchPos();
    data->isFixedStartWindowSize = mapButton.value("isFixedStartWindowSize")->isTrueSwitchPos();

    data->rateablyWindowScretch = rateablySize->value()/10;
    data->minimumSize = QSize(minimumSizeX->value(), minimumSizeY->value());
    data->startFixedWindowSize = QSize(startFixedWindowSizeX->value(), startFixedWindowSizeY->value());
    data->currentSize = tempSize;

    data->applySetting();
}

void GraphicView::setDefault()
{
    data->setDefault();

    mapButton.value("isMenuVisible")->setSwitcherPos(data->isMenuVisible);
    mapButton.value("isFixedSizeWindow")->setSwitcherPos(data->isFixedSizeWindow);
    mapButton.value("isAlwaysShowWindow")->setSwitcherPos(data->isAlwaysShowWindow);
    mapButton.value("isFloatLoginWindow")->setSwitcherPos(data->isFloatLoginWindow);
    mapButton.value("isChangeSizeRateably")->setSwitcherPos(data->isChangeSizeRateably);
    mapButton.value("isFixedStartWindowSize")->setSwitcherPos(data->isFixedStartWindowSize);

    rateablySize->setValue(data->rateablyWindowScretch*10);
    minimumSizeX->setValue(data->minimumSize.width());
    minimumSizeY->setValue(data->minimumSize.height());
    startFixedWindowSizeX->setValue(data->startFixedWindowSize.width());
    startFixedWindowSizeY->setValue(data->startFixedWindowSize.height());
}

void GraphicView::updatePoint()
{
    if (mapButton.value("isChangeSizeRateably")->isTrueSwitchPos()
            && (sender() == mapButton.value("isChangeSizeRateably")
                || sender() == rateablySize)){
            minimumSizeY->setValue(10*minimumSizeX->value()/rateablySize->value());
            minimumSizeShowLabelY->setNum(minimumSizeY->value());
            startFixedWindowSizeY->setValue(10*startFixedWindowSizeX->value()/rateablySize->value());
            startFixedShowLabelY->setNum(startFixedWindowSizeY->value());
            tempSize = QSize(tempSize.width(), (10*tempSize.width())/rateablySize->value());
    }
    else if (mapButton.value("isChangeSizeRateably")->isTrueSwitchPos()){
        if (sender() == minimumSizeX){
            minimumSizeY->setValue(10*minimumSizeX->value()/rateablySize->value());
            minimumSizeShowLabelY->setNum(minimumSizeY->value());
        }
        else if (sender() == startFixedWindowSizeX){
            startFixedWindowSizeY->setValue(10*startFixedWindowSizeX->value()/rateablySize->value());
            startFixedShowLabelY->setNum(startFixedWindowSizeY->value());
        }
    }
    if (sender() == minimumSizeX)
        startFixedWindowSizeX->setMinimum(minimumSizeX->value());
    else if (sender() == minimumSizeY)
        startFixedWindowSizeY->setMinimum(minimumSizeY->value());
    resizeContent();
}
