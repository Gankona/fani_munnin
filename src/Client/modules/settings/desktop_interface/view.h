#ifndef VIEW_H
#define VIEW_H

#include "mcontentwidget.h"
#include "data/settings_data.h"

#include "graphicview.h"
#include "hotkeyview.h"
#include "interfaceview.h"
#include "serverview.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QSettings>

class View : public MContentWidget
{
    Q_OBJECT
private:
    GraphicView *graphic;
    HotKeyView  *hotKey;
    InterfaceView *interfaces;
    ServerView *server;

    QSettings settings;
    Settings_data *data;

    int currentWidget;

public:
    View(Settings_data *data, QWidget *parent = nullptr);

signals:
    void goHome();

public slots:
    void setChooseWidget(int i);
    void backFromChild();
};

#endif // VIEW_H
