android{
    isEmpty(mobile.pri){
        CONFIG(debug, debug|release):   LIBS += -L$$PWD/../../../../debug/modules/ -lmobile
        CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../release/modules/ -lmobile
        INCLUDEPATH += $$PWD/../../mobile
        DEPENDPATH  += $$PWD/../../mobile
    }
}
