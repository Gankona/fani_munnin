#-------------------------------------------------
#
# Project created by QtCreator 2016-01-24T12:50:14
#
#-------------------------------------------------

QT       += widgets

TARGET = settings_mobile
TEMPLATE = lib

include($$PWD/../../../path.pri)

DESTDIR = $$LIBM_PATH/view

DEFINES += MOBILE_LIBRARY

SOURCES += mobile.cpp

HEADERS += mobile.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    mobile.pri
